import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AterPage } from '../pages/ater/ater';
import { PrefeiturasPage } from '../pages/prefeituras/prefeituras';

import { HttpModule } from '@angular/http';

import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { GraficosProvider } from '../providers/graficos/graficos';
import { Printer } from '@ionic-native/printer';


import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import exporting from 'highcharts/modules/offline-exporting.src';
import { MontarCsvProvider } from '../providers/montar-csv/montar-csv';

export function highchartsModules(){
  return [exporting];
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AterPage,
    PrefeiturasPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    ChartModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AterPage,
    PrefeiturasPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    FileOpener,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GraficosProvider,
    Printer,
    {provide: HIGHCHARTS_MODULES, useFactory: highchartsModules},
    MontarCsvProvider
  ]
})
export class AppModule {}

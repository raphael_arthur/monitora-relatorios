import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AterPage } from './ater';

@NgModule({
  declarations: [
    AterPage,
  ],
  imports: [
    IonicPageModule.forChild(AterPage),
  ],
})
export class AterPageModule {}

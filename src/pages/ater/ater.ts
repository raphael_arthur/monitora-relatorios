import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform  } from 'ionic-angular';
import { Http } from '@angular/http';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { GraficosProvider } from '../../providers/graficos/graficos';
import { MontarCsvProvider } from '../../providers/montar-csv/montar-csv';

import { Printer, PrintOptions } from '@ionic-native/printer';

import { Ater } from '../../models/modelo_ater';
import * as papa from 'papaparse';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

/**
 * Generated class for the AterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ater',
  templateUrl: 'ater.html',
})
export class AterPage {

  pdfObj = null;
  csvData: any[] = [];
  headerRow: any[] = [];

  conteudo:any = [];


  //Constantes do CSV:
  readonly os = 0;
  readonly cpf = 1;
  readonly entrevistador = 2;
  readonly data = 3;
  readonly uf = 4;
  readonly F = 5;
  readonly G = 6;
  readonly H = 7;
  readonly I = 8;
  readonly J = 9;
  readonly K = 10;
  readonly L = 11;
  readonly M = 12;
  readonly N = 13;
  readonly O = 14;
  readonly P = 15;
  readonly Q = 16;
  readonly R = 17;
  readonly S = 18;
  readonly T = 19;
  readonly U = 20;
  readonly V = 21;
  readonly X = 22;
  readonly Y = 23;
  readonly W = 24;
  readonly Z = 25;
  readonly AA = 26;
  readonly AB = 27;
  readonly AC = 28;
  readonly AD = 29;
  readonly AE = 30;
  readonly AF = 31;
  readonly AG = 32;
  readonly AH = 33;
  readonly AI = 34;
  readonly AJ = 35;
  readonly AK = 36;
  readonly AL = 37;
  readonly AM = 38;
  readonly AN = 39;
  readonly AO = 40;
  readonly AP = 41;
  readonly AQ = 42;
  readonly AR = 43;
  readonly AS = 44;
  readonly AT = 45;
  array_os: any;
  listaGraficos: any;
  docDefinition: { content: any[]; styles: { header: { fontSize: number; bold: boolean; }; subheader: { fontSize: number; bold: boolean; margin: number[]; }; story: { italic: boolean; alignment: string; width: string; }; }; };
  array_respostas: string[];
  supervisor_estadual:any;
  supervisor_nacional2: string[];
  array_nacional: any[];
  supervisor_nacional: {
  "Júlia": { //AL CE SE PI
    index: number; nomes: string[];
  }; "LUANA BEATRIZ DE JESUS VELOSO FREITAS": { //MG,ES,BA
    index: number; nomes: string[];
  }; "Gabriela": { //PE,PB,MA,RN
    index: number; nomes: string[];
  };
  };
  array_regional: any;
//+++++++++++++++++++++++++++++++++++++++++++++++++


  constructor(
    public csvProvider:MontarCsvProvider, 
    private printer: Printer, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private http: Http, 
    private plt: Platform, 
    private file: File, 
    private fileOpener: FileOpener, 
    public graficos:GraficosProvider
  ) 
  {
    this.array_os = [];
    this.array_regional = [];
    this.supervisor_estadual = {
      "José Cleto de Araújo Alves":{
        index:0,
        nomes:["Cinthia Guimarães de Oliveira", 
        "Guilherme Belmonte",
        "John Carlos da Silva Pinto",
        "Luciano Flamorion da Cunha Moreira",
        "Wellington Bezerra Soares",
        "Ivanilson Ferreira da Silva"],
      },
      "Bruno Xavier Gomes": {
        index:1,
        nomes:[
        "Antônio Carlos Cerqueira Borges",
        "Antonio Milton de Oliveira Silva",//APENAS 1 COLETA, TUDO NÃO OU NÃO INFORMADO
        "Geraldo Sérgio Silva Ferreira",
        "Hamilton Almeida Torres",
        "Hugo Fernando Barbosa da Silva",
        "José Raimundo Oliveira dos Santos",
        "Joseleine Mendes Barbosa Souza",
        "Monike Keila Sousa de Oliveira",
        "Paulo Henrique Carneiro de Oliveira",
        "Wellington dos Santos Dourado",
        "Antonio Carlos Cerqueira Borges"
      ]},
      "Paulo Matos Barreto":{
        index:2,
        nomes:[
        "Antônio Alberto Benevides S. Júnior",
        "Antônio Minelvino de Souza Neto" ,
        "Arnóbio Rodrigues da Silva" ,
        "Francisco das Chagas Martins Bezerra" ,
        "Johnathan Emanuel Pinto Bessa" ,
        "José Antônio Martins da Silva" ,
        "José Junior Gonçalves Noronha",
        "Manoel Vieira de Carvalho Filho" ,
        "Ramille Gonçalves de Souza" ,
        "Rodrigo de Andrade Rodrigues" ,
        "Yvon Saymon Canuto Vieira" ,
        "Antonio Alberto Benevides S. Júnior",
        "Ramille Gonçalves de Sousa",
        "Marciel da Silva Freire"
      ]},
      "Diego Capucho Cezana":{
        index:3,
        nomes:[
        "Daniele Batista Caetano",
        "Evaldo Nalli Ramos",
        "Sandra Madalena Valentim de Souza",
        'SANDRO SOARES TAVARES',
        'ADAILTON DUARTE LIMA',
        'ALEXSANDRO PEREIRA PEDROSA',
        'CELY MOURA AMARAL',
        'PAULO ROGÉRIO DE MEDEIROS SILVA',
        'SIDE CLAUDIO COSTA BARROS',
        
      ]},
      "Sandro Soares Tavares":{
        index:4,
        nomes:[
        'Adailton Duarte Lima',
        'Alexsandro Pereira Pedrosa',
        'Cely Moura Amaral',
        'Paulo Rogério de Medeiros Silva',
        'Side Claudio Costa Barros',
        "Silde Soares Tavares"
      ]},
      "Dinovan Queiroz de Almeida":{
        index:5,
        nomes:[
        'Adriana Viana Chagas',
        'Carlos Ruas Reis',
        'Fábio Ernani de Oliveira',
        'Fábio Luiz Becker',
        'Geraldo Aparecido Miguel Soares',
        'Hercules Vandy Durães da Fonseca',
        'João Eder Oliveira Almeida',
        'José Nilton Reis Santana',
        'Roselmiro Guimarães Entreportes',
        'Saulo Souza Matos',
        "Ezequiel Alves Martins"
      ]},
      "Julio Breno Vieira Lemos":{
        index:6,
        nomes:[
        'Adailson Bernardo dos Santos',
        'Bilac Soares de Oliveira',
        'Francisco de Luna Alves',
        'Leonel da Costa Azevedo',
        'Paulo Sergio Lins Dantas',
        'Robério Macêdo de Oliveira',
        'Victor Hugo Benevides Trigueiro Targino',
        'Lindinalva Oliveira de Paulo',
        'Paulo Lacerda de Oliveira',
        'Pablo Antonio de Sousa Monteiro',
        'Ana Palhano Freire Neta',
        'Jorismar dos Reis Fernandes'
      ]},
      "Edmar de Oliveira e Silva":{
        index:7,
        nomes:[
        'Antônio Florêncio Barros Medrado',
        'Eguimagno Rodrigues de Morais' ,
        'Esmar Esmeraldo Santos' ,
        'Francilene Pereira do Nascimento',
        'José Acevedo Alves Junior',
        'Júlio Cavalcante Lacerda Neto (Julinho)',
        'Kacia Amando da Silva' ,
        'Marcus Andre Pereira de Moura' ,
        'Rivanildo Adones dos Santos' ,
        'Wandebergue Luiz da Silva',
        'Luiz Canavello Neto'
      ]},
      "Bruno Fonseca Guerra":{
        index:8,
        nomes:[
        'Aemerson Rodrigues de Carvalho' ,
        'Ayrton Feitosa Santana' ,
        'Cicero Jose de Sousa',
        'Edilson de Souza Carvalho' ,
        'Hudson Maia de Carvalho',
        'Irineu Antonio da Costa' ,
        'Joilson Lustosa Silva Santana' ,
        'José Emilton Silva de Souza' ,
        'Wagner Sabino de Sousa Silva' ,
      ]},
      "Raimundo Thiago F. Moura de Araújo":{
        index:9,
        nomes:[
        'Eclecio Fernandes da Cunha' ,
        'Edjane Marinho da Costa' ,
        'Everton Silva Santos' ,
        'Fernanda Cinthia Fernandes Dantas',
        'Jonathas de Albuquerque Monteiro Bezerra',
        'José Jubenick Pereira da Silva' ,
        'José Leonilson Jácome Bezerra' ,
        'Lylyam Bibiana de Oliveira Fernandes' ,
        'Suelda Varela Caldas'
      ]},
      "Neydmar Loureiro dos Reis Lima":{
        index:10,
        nomes:[
        'Berenice Damazio Santos',
        'Reginaldo Pereira dos Santos',
        'Izaque Vieira dos Santos',
        'Wesley Santos Aragão',
        'Ana Maria Moura'
      ]}
    }
    this.supervisor_nacional2 = [
      'LUIS ANTÔNIO DA SILVA SOARES',
    ];
    this.array_nacional = [];

    this.supervisor_nacional = {
      "Júlia":{//AL CE SE PI
        index:0,
        nomes:["JOSÉ CLETO DE ARAÚJO ALVES", //AL 
        "PAULO MATOS BARRETO", //CE
        "NEYDMAR LOUREIRO DOS REIS LIMA", //SE
        "BRUNO FONSECA GUERRA", //PI
        ],
      },
      "LUANA BEATRIZ DE JESUS VELOSO FREITAS":{ //MG,ES,BA
        index:1,
        nomes:["DINOVAN QUEIROZ DE ALMEIDA",//MG 
        "DIEGO CAPUCHO CEZANA", //ES
        "BRUNO XAVIER GOMES", //BA
        ],
      },
      "Gabriela":{ //PE,PB,MA,RN
        index:2,
        nomes:["EDMAR DE OLIVEIRA E SILVA", //PE 
        "JULIO BRENO VIEIRA LEMOS", //PB
        "SANDRO SOARES TAVARES", //MA
        "RAIMUNDO THIAGO F. MOURA DE ARAÚJO",//RN
        ],
      },
    }
    this.readCsvData();
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad AterPage');
  }

  public readCsvData() {
    return new Promise ((resolve) => {
      this.http.get('assets/ater.csv')
      .subscribe(
      data => this.extractData(data),
      err => this.handleError(err)
      );
      resolve();
    });
  }

  private handleError(err) {
    //console.log('something went wrong: ', err);
  }
 
  trackByFn(index: any, item: any) {
    return index;
  }

  private extractData(res) {
    let csvData = res['_body'] || '';
    let parsedData = papa.parse(csvData, {delimiter:";"}).data;
 
    this.headerRow = parsedData[1];
 
    parsedData.splice(0, 2);
    this.csvData = parsedData;
    // //console.log(this.headerRow);
    // //console.log(this.csvData);
    var contador = 0;
    for(let i=0; i < this.csvData.length; i++)
    {
      let aterCabecalho = {
        os:this.csvData[i][this.os],
        cpf:this.csvData[i][this.cpf],
        entrevistador:this.csvData[i][this.entrevistador],
        data:this.csvData[i][this.data],
        uf:this.csvData[i][this.uf]
      };

      let Q14 = {
        bovinocultura:this.csvData[i][this.F],
        apicultura:this.csvData[i][this.G],
        caprinocultura:this.csvData[i][this.H],
        ovinocultura:this.csvData[i][this.I],
        suinocultura:this.csvData[i][this.J],
        avicultura:this.csvData[i][this.K],
        fruticultura:this.csvData[i][this.L],
        hortalicas:this.csvData[i][this.M],
        mandioca:this.csvData[i][this.N],
        milho:this.csvData[i][this.O],
        feijao:this.csvData[i][this.P],
        extrativismo:this.csvData[i][this.Q],
        ensilagem:this.csvData[i][this.R],
        pesca:this.csvData[i][this.S],
        artesanato:this.csvData[i][this.T],
        beneficiamento:this.csvData[i][this.U],
        outros:this.csvData[i][this.V],
      };

      let Q18 = {
        nova_forma_plantar:this.csvData[i][this.W],
        maquinas_agricolas:this.csvData[i][this.X],
        novo_animal:this.csvData[i][this.Y],
        nova_forma_alimentacao_animal:this.csvData[i][this.Z],
        alteracao_manejo:this.csvData[i][this.AA],
        uso_irrigacao:this.csvData[i][this.AB],
        cobertura_solo:this.csvData[i][this.AC],
        agroecologia:this.csvData[i][this.AD],
        outros:this.csvData[i][this.AE],
      };

      let Q19 = {
        armazenamento:this.csvData[i][this.AF],
        transporte:this.csvData[i][this.AG],
        processamento_alimentos:this.csvData[i][this.AH],
        embalagem:this.csvData[i][this.AI],
        canal_comercializacao:this.csvData[i][this.AJ],
        outros:this.csvData[i][this.AK],
      };

      let Q21 = {
        resposta:this.csvData[i][this.AL]
      };

      let Q22 = {
        resposta:this.csvData[i][this.AM]
      };

      let Q23 = {
        resposta:this.csvData[i][this.AN]
      };

      let Q31 = {
        resposta:this.csvData[i][this.AO]
      };

      let Q32 = {
        resposta:this.csvData[i][this.AP]
      };

      let Q34 = {
        resposta:this.csvData[i][this.AQ]
      };

      let Q35 = {
        resposta:this.csvData[i][this.AR]
      };

      let Q36 = {
        resposta:this.csvData[i][this.AS]
      };

      let Q38 = {
        resposta:this.csvData[i][this.AT]
      };

      if(i>0)
      {
        contador++;
        if((this.csvData[i][this.cpf] != this.csvData[i-1][this.cpf]) || (this.csvData[i][this.os] != this.csvData[i-1][this.os]))
        {
          let temp = new Ater(aterCabecalho);
          temp.addInfoEntrevista(Q14, Q18, Q19, Q21, Q22, Q23, Q31, Q32, Q34, Q35, Q36, Q38);
          this.array_os.push(temp);
        }
        else{
          let ult_index = this.array_os.length-1;
          // //console.log(this.array_os[ult_index]);
          this.array_os[ult_index].addInfoEntrevista(Q14, Q18, Q19, Q21, Q22, Q23, Q31, Q32, Q34, Q35, Q36, Q38);
        }
      }
      else if(i == 0)
      {
        contador++;
        let temp = new Ater(aterCabecalho);
        temp.addInfoEntrevista(Q14, Q18, Q19, Q21, Q22, Q23, Q31, Q32, Q34, Q35, Q36, Q38);
        this.array_os.push(temp);
      }      
    }

    // //console.log(this.array_os);

    // this.montarRegional().then(() =>{
    //   this.montarPDF2(0, "REGIONAL", this.array_regional);
    //   // this.montarNacionalParcial().then(() =>{
    //   //   this.montarPDF2(0, "NACIONAL", this.array_nacional);
    //   // });
    // });
    this.montarPDF(0);

  }

  public montarPDF(itemAtual:number)
  {
    var self = this;
    if(itemAtual < self.array_os.length)
    // if(itemAtual == 0)
     {
      let row = self.array_os[itemAtual];
        let cabecalho = `RELATÓRIO NÍVEL LOCAL - Questionário Inovação e ATER 
        (${row.entrevistador} OS: ${row.os})`;
        // let cabecalho = `RELATÓRIO NÍVEL ESTADUAL (${row.info_os.nome_entrevistador}`;
        let blocoA = `
          Q01. Quais as principais atividades produtivas realizadas pelos agricultores?  
        `;
        let blocoB = `
          Q02. Nos últimos 3 anos, houve alguma mudança importante na forma de realizar as atividades produtivas?   
        `;
        let blocoC = `
          Q03. Nos últimos 3 anos, as famílias adotaram alguma mudança importante na forma de “pós-produção/pós-colheita”?   
        `;
        let blocoD = `
          Q04. Nos últimos 3 anos, quantos entrevistados desistiram de iniciar uma atividade? 
        `;
        let blocoE = `
          Q05. Nos últimos 3 anos, quantos receberam assistência técnica?   
        `;
        let blocoF = `
          Q06. Quantos entrevistados desejam mudar a principal atividade econômica da propriedade?   
        `;
        let blocoG = `
          Q07. Quantos são beneficiários do Bolsa Família?  
        `;
        // let blocoH = `
        //   Q32. Quantos conhecem o Projeto Dom Hélder Câmera?  
        // `;
        let blocoI = `
          Q08. Quantos participam de algum dos Projetos do FIDA?
        `;
        let blocoJ = `
          Q09. Quantos participaram de alguma atividade do Projeto Dom Hélder Câmara entre os anos de 2001 e 2015?  
        `;
        let blocoK = `
          Q11. Quantos participam de alguma instância política/representativa? (Sindicatos, Cooperativas, Associações, Movimentos Sociais, Colegiados, Conselhos, Partidos Políticos, e etc)   
        `;
        let blocoL = `
          Q12. Quantos dos entrevistados já acessaram o benefício de Assistência Técnica e Extensão Rural – ATER?  
        `;
        self.plotarGrafico(itemAtual, self.array_os).then(() => { 
          let resposta18 = this.array_respostas[0];
          let resposta19 = this.array_respostas[1]
          self.conteudo.push(
            { text: cabecalho, style: 'header', alignment: 'center'  },
            { text: blocoA, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[0], width: 550, alignment: 'center'  },
            { text: blocoB, style: 'story', alignment: 'left'},
            { text: resposta18, style: 'story', alignment: 'left'},
            // { image:this.listaGraficos[1], width: 550, alignment: 'center',   },
            { text: blocoC, style: 'story', alignment: 'left',},
            { text: resposta19, style: 'story', alignment: 'left'},
            // { image:this.listaGraficos[2], width: 550, alignment: 'center' ,pageBreak: 'after'     },
            { text: blocoD, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[3], width: 600, alignment: 'center',  },
            { text: blocoE, style: 'story', alignment: 'left', pageBreak:'before'},
            { image:this.listaGraficos[4], width: 600, alignment: 'center',  },
            { text: blocoF, style: 'story', alignment: 'left',},
            { image:this.listaGraficos[5], width: 600, alignment: 'center' ,},
            { text: blocoG, style: 'story', alignment: 'left', pageBreak:'before'},
            { image:this.listaGraficos[6], width: 600, alignment: 'center'   },
            // { text: blocoH, style: 'story', alignment: 'left'},
            // { image:this.listaGraficos[7], width: 600, alignment: 'center',  },
            { text: blocoI, style: 'story', alignment: 'left',pageBreak:'before'},
            { image:this.listaGraficos[8], width: 600, alignment: 'center' ,},
            { text: blocoJ, style: 'story', alignment: 'left',},
            { image:this.listaGraficos[9], width: 600, alignment: 'center',   },
            { text: blocoK, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[10], width: 600, alignment: 'center',},
            { text: blocoL, style: 'story', alignment: 'left',pageBreak:'before'},
            { image:this.listaGraficos[11], width: 600, alignment: 'center',  pageBreak: 'after'},
            // { text:'', },
          );
            self.listaGraficos = [];
            itemAtual++;
            self.montarPDF(itemAtual);
        });
     }
     else
     {
       this.montarDocDefinition();
     }

     
  }

  private plotarGrafico(itemAtual:number, array:any)
  {
    return new Promise ((resolve) => {
      var self = this;

      //Questão 14:
      //Contador quantas respostas SIM foram identificadas
      let Q14_length  = [
        {tipo:'bovinocultura', valor: array[itemAtual].Q14.bovinocultura.filter(item => item === "SIM").length/array[itemAtual].Q14.bovinocultura.length*100,},
        {tipo:'apicultura', valor: array[itemAtual].Q14.apicultura.filter(item => item === "SIM").length/array[itemAtual].Q14.apicultura.length*100,},
        {tipo:'caprinocultura', valor: array[itemAtual].Q14.caprinocultura.filter(item => item === "SIM").length/array[itemAtual].Q14.caprinocultura.length*100,},
        {tipo:'ovinocultura', valor: array[itemAtual].Q14.ovinocultura.filter(item => item === "SIM").length/array[itemAtual].Q14.ovinocultura.length*100},
        {tipo:'suinocultura', valor: array[itemAtual].Q14.suinocultura.filter(item => item === "SIM").length/array[itemAtual].Q14.suinocultura.length*100},
        {tipo:'avicultura', valor: array[itemAtual].Q14.avicultura.filter(item => item === "SIM").length/array[itemAtual].Q14.avicultura.length*100},
        {tipo:'fruticultura', valor: array[itemAtual].Q14.fruticultura.filter(item => item === "SIM").length/array[itemAtual].Q14.fruticultura.length*100},
        {tipo:'hortalicas', valor: array[itemAtual].Q14.hortalicas.filter(item => item === "SIM").length/array[itemAtual].Q14.hortalicas.length*100},
        {tipo:'mandioca', valor: array[itemAtual].Q14.mandioca.filter(item => item === "SIM").length/array[itemAtual].Q14.mandioca.length*100},
        {tipo:'milho', valor: array[itemAtual].Q14.milho.filter(item => item === "SIM").length/array[itemAtual].Q14.milho.length*100},
        {tipo:'feijao', valor: array[itemAtual].Q14.feijao.filter(item => item === "SIM").length/array[itemAtual].Q14.feijao.length*100},
        {tipo:'extrativismo', valor: array[itemAtual].Q14.extrativismo.filter(item => item === "SIM").length/array[itemAtual].Q14.extrativismo.length*100},
        {tipo:'ensilagem', valor: array[itemAtual].Q14.ensilagem.filter(item => item === "SIM").length/array[itemAtual].Q14.ensilagem.length*100},
        {tipo:'pesca', valor: array[itemAtual].Q14.pesca.filter(item => item === "SIM").length/array[itemAtual].Q14.pesca.length*100},
        {tipo:'artesanato', valor: array[itemAtual].Q14.artesanato.filter(item => item === "SIM").length/array[itemAtual].Q14.artesanato.length*100},
        {tipo:'beneficiamento', valor: array[itemAtual].Q14.beneficiamento.filter(item => item === "SIM").length/array[itemAtual].Q14.beneficiamento.length*100},
        {tipo:'Outros', valor: array[itemAtual].Q14.outros.filter(item => item === "SIM").length/array[itemAtual].Q14.outros.length*100},
      ];  
    
      Q14_length = Q14_length.sort((a,b) => {
          if(a.valor < b.valor){
            return 1;
          }
          if(a.valor > b.valor){
            return -1;
          }
          return 0;
      });
      Q14_length = Q14_length.filter(item => item.valor > 0);
    let dataQ14 = Q14_length.map(a => (a.valor));
    let labelsQ14 = Q14_length.map(a => (a.tipo));
    // //console.log(labelsQ14);
    // //console.log(dataQ14);

    let dataGraficoBlocoA = {
      div:"graficoBlocoA",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      eixoY: "% de presença",
      eixoX: labelsQ14,
      series: [{
        type: undefined,
        name: '%',
        data: dataQ14

      }],
    };
    var graficoBlocoA = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoA);

    //------------------------------------------
    //-----------------Questão 18---------------
    let Q18_length  = [
      {tipo:'Nova forma de plantar', valor: array[itemAtual].Q18.nova_forma_plantar.filter(item => item === "SIM").length/array[itemAtual].Q18.nova_forma_plantar.length*100,},
      {tipo:'Utilização de máquinas agrícolas', valor: array[itemAtual].Q18.maquinas_agricolas.filter(item => item === "SIM").length/array[itemAtual].Q18.maquinas_agricolas.length*100,},
      {tipo:'Uso de nova variedade/espécie animal', valor: array[itemAtual].Q18.novo_animal.filter(item => item === "SIM").length/array[itemAtual].Q18.novo_animal.length*100,},
      {tipo:'Nova forma de alimentação animal', valor: array[itemAtual].Q18.nova_forma_alimentacao_animal.filter(item => item === "SIM").length/array[itemAtual].Q18.nova_forma_alimentacao_animal.length*100},
      {tipo:'Alteração no manejo', valor: array[itemAtual].Q18.alteracao_manejo.filter(item => item === "SIM").length/array[itemAtual].Q18.alteracao_manejo.length*100},
      {tipo:'Uso de irrigação', valor: array[itemAtual].Q18.uso_irrigacao.filter(item => item === "SIM").length/array[itemAtual].Q18.uso_irrigacao.length*100},
      {tipo:'Cobertura do solo', valor: array[itemAtual].Q18.cobertura_solo.filter(item => item === "SIM").length/array[itemAtual].Q18.cobertura_solo.length*100},
      {tipo:'Agroecologia', valor: array[itemAtual].Q18.agroecologia.filter(item => item === "SIM").length/array[itemAtual].Q18.agroecologia.length*100},
      {tipo:'Outros', valor: array[itemAtual].Q18.outros.filter(item => item === "SIM").length/array[itemAtual].Q18.outros.length*100},
      
    ];  
  
    Q18_length = Q18_length.sort((a,b) => {
        if(a.valor < b.valor){
          return 1;
        }
        if(a.valor > b.valor){
          return -1;
        }
        return 0;
    });
    // Q18_length = Q18_length.filter(item => item.valor > 0);
    let Q18_filtro = Q18_length.filter(a => (a.valor > 0));
  let dataQ18 = Q18_length.map(a => (a.valor));
  let labelsQ18 = Q18_length.map(a => (a.tipo));
  // //console.log(labelsQ18);
  // //console.log(dataQ18);

  let dataGraficoBlocoB = {
    div:"graficoBlocoB",
    titulo:"",
    subtitulo:"",
    // eixoX: "dinamico conforme tabela TODO",
    eixoY: "% de respostas SIM",
    eixoX: labelsQ18,
    series: [{
      type: undefined,
      name: '%',
      data: dataQ18

    }],
  };
  let resposta = dataQ18.filter(a => (a>0));
  var respostaBlocoB:string;
  // if(resposta.length > 0) respostaBlocoB = "Sim.";
  // else respostaBlocoB = "Não.";
  if(Q18_filtro.length > 0)
  {
    respostaBlocoB = "Sim: ";
    for(let item of Q18_filtro)
    {
      respostaBlocoB = respostaBlocoB + item.tipo + "; ";
    }
  }
  else respostaBlocoB = "Não.";
   
  var graficoBlocoB = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoB);

  //---------------------------------------------------------------------------------------------------------
  //-----------QUESTÃO 19--------------------------------
  let Q19_length  = [
    {tipo:'Nova forma de armazenamento', valor: array[itemAtual].Q19.armazenamento.filter(item => item === "SIM").length/array[itemAtual].Q19.armazenamento.length*100,},
    {tipo:'Nova forma de transportar a produção', valor: array[itemAtual].Q19.transporte.filter(item => item === "SIM").length/array[itemAtual].Q19.transporte.length*100,},
    {tipo:'Processamento de alimentos (doces, farinhas, etc)', valor: array[itemAtual].Q19.processamento_alimentos.filter(item => item === "SIM").length/array[itemAtual].Q19.processamento_alimentos.length*100,},
    {tipo:'Nova embalagem', valor: array[itemAtual].Q19.embalagem.filter(item => item === "SIM").length/array[itemAtual].Q19.embalagem.length*100},
    {tipo:'Novo canal de comercialização', valor: array[itemAtual].Q19.canal_comercializacao.filter(item => item === "SIM").length/array[itemAtual].Q19.canal_comercializacao.length*100},
    {tipo:'Outros', valor: array[itemAtual].Q19.outros.filter(item => item === "SIM").length/array[itemAtual].Q19.outros.length*100},
  ];  

  Q19_length = Q19_length.sort((a,b) => {
      if(a.valor < b.valor){
        return 1;
      }
      if(a.valor > b.valor){
        return -1;
      }
      return 0;
  });
  // Q18_length = Q18_length.filter(item => item.valor > 0);
  let Q19_filtro = Q19_length.filter(a => (a.valor > 0));
  let dataQ19 = Q19_length.map(a => (a.valor));
  let labelsQ19 = Q19_length.map(a => (a.tipo));
  //console.log(dataQ19);
  //console.log(labelsQ19);

  let dataGraficoBlocoC = {
    div:"graficoBlocoC",
    titulo:"",
    subtitulo:"",
    // eixoX: "dinamico conforme tabela TODO",
    eixoY: "% de respostas SIM",
    eixoX: labelsQ19,
    series: [{
      type: undefined,
      name: '%',
      data: dataQ19

    }],
  };
  let resposta19 = dataQ19.filter(a => (a>0));
  var respostaBlocoC:string;
  // if(resposta19.length > 0) respostaBlocoC = "Sim.";
  // else respostaBlocoC = "Não.";
  if(Q19_filtro.length > 0)
  {
    respostaBlocoC = "Sim: ";
    for(let item of Q19_filtro)
    {
      respostaBlocoC = respostaBlocoC + item.tipo + "; ";
    }
  }
  else respostaBlocoC = "Não.";
  var graficoBlocoC = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoC);
  //--------------------------------------------------------------------------------
  //---------------QUESTÃO 21-------------------------
  // let Q21_length  = 
  //   {tipo:'Respostas', valor: array[itemAtual].Q21.filter(item => item === "SIM").length/array[itemAtual].Q21.length*100,};  

  // let dataQ21 = Q21_length.valor;
  // let labelsQ21 = Q21_length.tipo;
  // //console.log(dataQ21);
  // //console.log(labelsQ21);

  // let dataGraficoBlocoD = {
  //   div:"graficoBlocoD",
  //   titulo:"",
  //   subtitulo:"",
  //   // eixoX: "dinamico conforme tabela TODO",
  //   eixoY: "% de respostas SIM",
  //   eixoX: [labelsQ21],
  //   series: [{
  //     type: undefined,
  //     name: '%',
  //     data: [dataQ21]

  //   }],
  // };
  // var graficoBlocoD = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoD);
  let Q21_length  = [
    {tipo:'Sim, desistiu', valor: array[itemAtual].Q21.filter(item => item === "SIM").length/array[itemAtual].Q21.length*100,},
    {tipo:'Permanece', valor: array[itemAtual].Q21.filter(item => item === "NÃO").length/array[itemAtual].Q21.length*100,},
    {tipo:'Não informado', valor: array[itemAtual].Q21.filter(item => item === "").length/array[itemAtual].Q21.length*100,},
  ];  

  let dataQ21 = Q21_length.map(a => (a.valor));
  let labelsQ21 = Q21_length.map(a => (a.tipo));
  //console.log(dataQ21);
  //console.log(labelsQ21);

  let dataGraficoBlocoD:any;
  if(dataQ21[2] > 0)
  {
    dataGraficoBlocoD = {
      div:"graficoBlocoD",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ21[0],
            y: dataQ21[0]
          },
          {
            type: undefined,
            name: labelsQ21[1],
            y: dataQ21[1]
          },
          {
            type: undefined,
            name: labelsQ21[2],
            y: dataQ21[2]
          }],
      }]
    };
  }
  else{
    dataGraficoBlocoD = {
      div:"graficoBlocoD",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ21[0],
            y: dataQ21[0]
          },
          {
            type: undefined,
            name: labelsQ21[1],
            y: dataQ21[1]
          }],
      }]
    };
  }
  
  var graficoBlocoD = this.graficos.plotarGraficoPizza(dataGraficoBlocoD);
  //--------------------------------------------------------------------------------
  //---------------QUESTÃO 22-------------------------
  // let Q22_length  = 
  //   {tipo:'Respostas', valor: array[itemAtual].Q22.filter(item => item === "SIM").length/array[itemAtual].Q22.length*100,};  

  // let dataQ22 = Q22_length.valor;
  // let labelsQ22 = Q22_length.tipo;
  // //console.log(dataQ22);
  // //console.log(labelsQ22);

  // let dataGraficoBlocoE = {
  //   div:"graficoBlocoE",
  //   titulo:"",
  //   subtitulo:"",
  //   // eixoX: "dinamico conforme tabela TODO",
  //   eixoY: "% de respostas SIM",
  //   eixoX: [labelsQ22],
  //   series: [{
  //     type: undefined,
  //     name: '%',
  //     data: [dataQ22]

  //   }],
  // };
  let Q22_length  = [
    {tipo:'Sim', valor: array[itemAtual].Q22.filter(item => item === "SIM").length/array[itemAtual].Q22.length*100,},
    {tipo:'Não recebeu', valor: array[itemAtual].Q22.filter(item => item === "NÃO").length/array[itemAtual].Q22.length*100,},
    {tipo:'Não informado', valor: array[itemAtual].Q22.filter(item => item === "").length/array[itemAtual].Q22.length*100,},
  ];  

  let dataQ22 = Q22_length.map(a => (a.valor));
  let labelsQ22 = Q22_length.map(a => (a.tipo));
  //console.log(dataQ22);
  //console.log(labelsQ22);

  let dataGraficoBlocoE:any;
  if(dataQ22[2] > 0)
  {
    dataGraficoBlocoE = {
      div:"graficoBlocoE",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ22[0],
            y: dataQ22[0]
          },
          {
            type: undefined,
            name: labelsQ22[1],
            y: dataQ22[1]
          },
          {
            type: undefined,
            name: labelsQ22[2],
            y: dataQ22[2]
          }],
      }]
    };
  }
  else{
    dataGraficoBlocoE = {
      div:"graficoBlocoE",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ22[0],
            y: dataQ22[0]
          },
          {
            type: undefined,
            name: labelsQ22[1],
            y: dataQ22[1]
          }],
      }]
    };
  }
  
  var graficoBlocoE = this.graficos.plotarGraficoPizza(dataGraficoBlocoE);

  //--------------------------------------------------------------------------------
  //---------------QUESTÃO 23-------------------------
  // let Q23_length  = 
  //   {tipo:'Respostas', valor: array[itemAtual].Q23.filter(item => item === "SIM").length/array[itemAtual].Q23.length*100,};  

  // let dataQ23 = Q23_length.valor;
  // let labelsQ23 = Q23_length.tipo;
  // //console.log(dataQ23);
  // //console.log(labelsQ23);

  // let dataGraficoBlocoF = {
  //   div:"graficoBlocoF",
  //   titulo:"",
  //   subtitulo:"",
  //   // eixoX: "dinamico conforme tabela TODO",
  //   eixoY: "% de respostas SIM",
  //   eixoX: [labelsQ23],
  //   series: [{
  //     type: undefined,
  //     name: '%',
  //     data: [dataQ23]

  //   }],
  // };
  // var graficoBlocoF = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoF);
  let Q23_length  = [
    {tipo:'Sim', valor: array[itemAtual].Q23.filter(item => item === "SIM").length/array[itemAtual].Q23.length*100,},
    {tipo:'Não pretende', valor: array[itemAtual].Q23.filter(item => item === "NÃO").length/array[itemAtual].Q23.length*100,},
    {tipo:'Não informado', valor: array[itemAtual].Q23.filter(item => item === "").length/array[itemAtual].Q23.length*100,},
  ];  

  let dataQ23 = Q23_length.map(a => (a.valor));
  let labelsQ23 = Q23_length.map(a => (a.tipo));
  //console.log(dataQ23);
  //console.log(labelsQ23);

  let dataGraficoBlocoF:any;
  if(dataQ23[2] > 0)
  {
    dataGraficoBlocoF = {
      div:"graficoBlocoF",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ23[0],
            y: dataQ23[0]
          },
          {
            type: undefined,
            name: labelsQ23[1],
            y: dataQ23[1]
          },
          {
            type: undefined,
            name: labelsQ23[2],
            y: dataQ23[2]
          }],
      }]
    };
  }
  else{
    dataGraficoBlocoF = {
      div:"graficoBlocoF",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ23[0],
            y: dataQ23[0]
          },
          {
            type: undefined,
            name: labelsQ23[1],
            y: dataQ23[1]
          }],
      }]
    };
  }
  
  var graficoBlocoF = this.graficos.plotarGraficoPizza(dataGraficoBlocoF);

  //--------------------------------------------------------------------------------
  //---------------QUESTÃO 31-------------------------
  let Q31_length  = [
    {tipo:'Sim', valor: array[itemAtual].Q31.filter(item => item === "SIM").length/array[itemAtual].Q31.length*100,},
    {tipo:'Não', valor: array[itemAtual].Q31.filter(item => item === "NÃO").length/array[itemAtual].Q31.length*100,},
    {tipo:'Outro', valor: array[itemAtual].Q31.filter(item => item === "OUTRO").length/array[itemAtual].Q31.length*100,},
    {tipo:'Não informado', valor: array[itemAtual].Q31.filter(item => item === "").length/array[itemAtual].Q31.length*100,},
  ];  

  let dataQ31 = Q31_length.map(a => (a.valor));
  let labelsQ31 = Q31_length.map(a => (a.tipo));
  //console.log(dataQ31);
  //console.log(labelsQ31);

  let dataGraficoBlocoG:any;
  if(dataQ31[3] > 0)
  {
    dataGraficoBlocoG = {
      div:"graficoBlocoG",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ31[0],
            y: dataQ31[0]
          },
          {
            type: undefined,
            name: labelsQ31[1],
            y: dataQ31[1]
          },
          {
            type: undefined,
            name: labelsQ31[2],
            y: dataQ31[2]
          },
          {
            type: undefined,
            name: labelsQ31[3],
            y: dataQ31[3]
        }],
      }]
    };
  }
  else{
    dataGraficoBlocoG = {
      div:"graficoBlocoG",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ31[0],
            y: dataQ31[0]
          },
          {
            type: undefined,
            name: labelsQ31[1],
            y: dataQ31[1]
          },
          {
            type: undefined,
            name: labelsQ31[2],
            y: dataQ31[2]
          }],
      }]
    };
  }
  
  var graficoBlocoG = this.graficos.plotarGraficoPizza(dataGraficoBlocoG);

  //--------------------------------------------------------------------------------
  //---------------QUESTÃO 32-------------------------
  // let Q32_length  = 
  //   {tipo:'Respostas', valor: array[itemAtual].Q32.filter(item => (item != "NÃO" && item != "")).length/array[itemAtual].Q32.length*100,};  

  // let dataQ32 = Q32_length.valor;
  // let labelsQ32 = Q32_length.tipo;
  // //console.log(dataQ32);
  // //console.log(labelsQ32);

  // let dataGraficoBlocoH = {
  //   div:"graficoBlocoH",
  //   titulo:"",
  //   subtitulo:"",
  //   // eixoX: "dinamico conforme tabela TODO",
  //   eixoY: "% de respostas SIM",
  //   eixoX: [labelsQ32],
  //   series: [{
  //     type: undefined,
  //     name: '%',
  //     data: [dataQ32]

  //   }],
  // };
  // var graficoBlocoH = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoH);
  let Q32_length  = [
    {tipo:'Sim, participa.', valor: array[itemAtual].Q32.filter(item => item === "SIM").length/array[itemAtual].Q32.length*100,},
    {tipo:'Não participa.', valor: array[itemAtual].Q32.filter(item => item === "NÃO").length/array[itemAtual].Q32.length*100,},
    {tipo:'Não informado', valor: array[itemAtual].Q32.filter(item => item === "").length/array[itemAtual].Q32.length*100,},
  ];  

  let dataQ32 = Q32_length.map(a => (a.valor));
  let labelsQ32 = Q32_length.map(a => (a.tipo));
  //console.log(dataQ32);
  //console.log(labelsQ32);

  let dataGraficoBlocoH:any;
  if(dataQ32[2] > 0)
  {
    dataGraficoBlocoH = {
      div:"graficoBlocoH",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ32[0],
            y: dataQ32[0]
          },
          {
            type: undefined,
            name: labelsQ32[1],
            y: dataQ32[1]
          },
          {
            type: undefined,
            name: labelsQ32[2],
            y: dataQ32[2]
          }],
      }]
    };
  }
  else{
    dataGraficoBlocoH = {
      div:"graficoBlocoH",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ32[0],
            y: dataQ32[0]
          },
          {
            type: undefined,
            name: labelsQ32[1],
            y: dataQ32[1]
          }],
      }]
    };
  }
  
  var graficoBlocoH = this.graficos.plotarGraficoPizza(dataGraficoBlocoH);
  //--------------------------------------------------------------------------------
  //---------------QUESTÃO 34-------------------------
  // let Q34_length  = 
  //   {tipo:'Respostas', valor: array[itemAtual].Q34.filter(item => (item ==='SIM')).length/array[itemAtual].Q34.length*100,};  

  // let dataQ34 = Q34_length.valor;
  // let labelsQ34 = Q34_length.tipo;
  // //console.log(dataQ34);
  // //console.log(labelsQ34);

  // let dataGraficoBlocoI = {
  //   div:"graficoBlocoI",
  //   titulo:"",
  //   subtitulo:"",
  //   // eixoX: "dinamico conforme tabela TODO",
  //   eixoY: "% de respostas SIM",
  //   eixoX: [labelsQ34],
  //   series: [{
  //     type: undefined,
  //     name: '%',
  //     data: [dataQ34]

  //   }],
  // };
  // var graficoBlocoI = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoI);
  let Q34_length  = [
    {tipo:'Sim, participa.', valor: array[itemAtual].Q34.filter(item => item === "SIM").length/array[itemAtual].Q34.length*100,},
    {tipo:'Não participa.', valor: array[itemAtual].Q34.filter(item => item === "NÃO").length/array[itemAtual].Q34.length*100,},
    {tipo:'Não informado', valor: array[itemAtual].Q34.filter(item => item === "").length/array[itemAtual].Q34.length*100,},
  ];  

  let dataQ34 = Q34_length.map(a => (a.valor));
  let labelsQ34 = Q34_length.map(a => (a.tipo));
  //console.log(dataQ34);
  //console.log(labelsQ34);

  let dataGraficoBlocoI:any;
  if(dataQ34[2] > 0)
  {
    dataGraficoBlocoI = {
      div:"graficoBlocoI",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ34[0],
            y: dataQ34[0]
          },
          {
            type: undefined,
            name: labelsQ34[1],
            y: dataQ34[1]
          },
          {
            type: undefined,
            name: labelsQ34[2],
            y: dataQ34[2]
          }],
      }]
    };
  }
  else{
    dataGraficoBlocoI = {
      div:"graficoBlocoI",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ34[0],
            y: dataQ34[0]
          },
          {
            type: undefined,
            name: labelsQ34[1],
            y: dataQ34[1]
          }],
      }]
    };
  }
  
  var graficoBlocoI = this.graficos.plotarGraficoPizza(dataGraficoBlocoI);

  //--------------------------------------------------------------------------------
  //---------------QUESTÃO 35-------------------------
  // let Q35_length  = 
  //   {tipo:'Respostas', valor: array[itemAtual].Q35.filter(item => (item ==='SIM')).length/array[itemAtual].Q35.length*100,};  

  // let dataQ35 = Q35_length.valor;
  // let labelsQ35 = Q35_length.tipo;
  // //console.log(dataQ35);
  // //console.log(labelsQ35);

  // let dataGraficoBlocoJ = {
  //   div:"graficoBlocoJ",
  //   titulo:"",
  //   subtitulo:"",
  //   // eixoX: "dinamico conforme tabela TODO",
  //   eixoY: "% de respostas SIM",
  //   eixoX: [labelsQ35],
  //   series: [{
  //     type: undefined,
  //     name: '%',
  //     data: [dataQ35]

  //   }],
  // };
  // var graficoBlocoJ = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoJ);
  let Q35_length  = [
    {tipo:'Sim, participou.', valor: array[itemAtual].Q35.filter(item => item === "SIM").length/array[itemAtual].Q35.length*100,},
    {tipo:'Não participou.', valor: array[itemAtual].Q35.filter(item => item === "NÃO").length/array[itemAtual].Q35.length*100,},
    {tipo:'Não informado', valor: array[itemAtual].Q35.filter(item => item === "").length/array[itemAtual].Q35.length*100,},
  ];  

  let dataQ35 = Q35_length.map(a => (a.valor));
  let labelsQ35 = Q35_length.map(a => (a.tipo));
  //console.log(dataQ35);
  //console.log(labelsQ35);

  let dataGraficoBlocoJ:any;
  if(dataQ35[2] > 0)
  {
    dataGraficoBlocoJ = {
      div:"graficoBlocoJ",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ35[0],
            y: dataQ35[0]
          },
          {
            type: undefined,
            name: labelsQ35[1],
            y: dataQ35[1]
          },
          {
            type: undefined,
            name: labelsQ35[2],
            y: dataQ35[2]
          }],
      }]
    };
  }
  else{
    dataGraficoBlocoJ = {
      div:"graficoBlocoJ",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ35[0],
            y: dataQ35[0]
          },
          {
            type: undefined,
            name: labelsQ35[1],
            y: dataQ35[1]
          }],
      }]
    };
  }
  
  var graficoBlocoJ = this.graficos.plotarGraficoPizza(dataGraficoBlocoJ);

  //--------------------------------------------------------------------------------
  //---------------QUESTÃO 36-------------------------
  // let Q36_length  = 
  //   {tipo:'Respostas K', valor: array[itemAtual].Q36.filter(item => (item ==='SIM')).length/array[itemAtual].Q36.length*100,};  

  // let dataQ36 = Q36_length.valor;
  // let labelsQ36 = Q36_length.tipo;
  // //console.log(dataQ36);
  // //console.log(labelsQ36);

  // let dataGraficoBlocoK = {
  //   div:"graficoBlocoK",
  //   titulo:"",
  //   subtitulo:"",
  //   // eixoX: "dinamico conforme tabela TODO",
  //   eixoY: "% de respostas SIM",
  //   eixoX: [labelsQ36],
  //   series: [{
  //     type: undefined,
  //     name: '%',
  //     data: [dataQ36]

  //   }],
  // };
  // var graficoBlocoK = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoK);
  let Q36_length  = [
    {tipo:'Sim', valor: array[itemAtual].Q36.filter(item => item === "SIM").length/array[itemAtual].Q36.length*100,},
    {tipo:'Não', valor: array[itemAtual].Q36.filter(item => item === "NÃO").length/array[itemAtual].Q36.length*100,},
    {tipo:'Não informado', valor: array[itemAtual].Q36.filter(item => item === "").length/array[itemAtual].Q36.length*100,},
  ];  

  let dataQ36 = Q36_length.map(a => (a.valor));
  let labelsQ36 = Q36_length.map(a => (a.tipo));
  //console.log(dataQ36);
  //console.log(labelsQ36);

  let dataGraficoBlocoK:any;
  if(dataQ36[2] > 0)
  {
    dataGraficoBlocoK = {
      div:"graficoBlocoK",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ36[0],
            y: dataQ36[0]
          },
          {
            type: undefined,
            name: labelsQ36[1],
            y: dataQ36[1]
          },
          {
            type: undefined,
            name: labelsQ36[2],
            y: dataQ36[2]
          }],
      }]
    };
  }
  else{
    dataGraficoBlocoK = {
      div:"graficoBlocoK",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ36[0],
            y: dataQ36[0]
          },
          {
            type: undefined,
            name: labelsQ36[1],
            y: dataQ36[1]
          }],
      }]
    };
  }
  
  var graficoBlocoK = this.graficos.plotarGraficoPizza(dataGraficoBlocoK);

  //--------------------------------------------------------------------------------
  //---------------QUESTÃO 38-------------------------
  let Q38_length  = [
    {tipo:'Sim', valor: array[itemAtual].Q38.filter(item => item === "SIM").length/array[itemAtual].Q38.length*100,},
    {tipo:'Não', valor: array[itemAtual].Q38.filter(item => item === "NÃO").length/array[itemAtual].Q38.length*100,},
    {tipo:'Não informado', valor: array[itemAtual].Q38.filter(item => item === "").length/array[itemAtual].Q38.length*100,},
  ];  

  let dataQ38 = Q38_length.map(a => (a.valor));
  let labelsQ38 = Q38_length.map(a => (a.tipo));
  //console.log(dataQ38);
  //console.log(labelsQ38);

  let dataGraficoBlocoL:any;
  if(dataQ38[2] > 0)
  {
    dataGraficoBlocoL = {
      div:"graficoBlocoL",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ38[0],
            y: dataQ38[0]
          },
          {
            type: undefined,
            name: labelsQ38[1],
            y: dataQ38[1]
          },
          {
            type: undefined,
            name: labelsQ38[2],
            y: dataQ38[2]
          }],
      }]
    };
  }
  else
  {
    dataGraficoBlocoL = {
      div:"graficoBlocoL",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ38[0],
            y: dataQ38[0]
          },
          {
            type: undefined,
            name: labelsQ38[1],
            y: dataQ38[1]
          }],
      }]
    };
  }
  var graficoBlocoL = this.graficos.plotarGraficoPizza(dataGraficoBlocoL);

  let array_plots = [graficoBlocoA, graficoBlocoB, graficoBlocoC, graficoBlocoD, graficoBlocoE, graficoBlocoF, graficoBlocoG, graficoBlocoH, graficoBlocoI, graficoBlocoJ, graficoBlocoK, graficoBlocoL];
  this.array_respostas = [respostaBlocoB, respostaBlocoC];  
  this.graficos.converterSVGtoPNG(array_plots, 0).then((data) =>{
      //  //console.log(data);
      this.listaGraficos = data;
      resolve();
    });
});
  }

  public montarDocDefinition()
  {
    this.docDefinition = {
      content: [
        this.conteudo
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
          
        }
      }
    }
  }

  public getDoc()
  {
    return this.docDefinition;
  }

  downloadPdf() {
    let docDefinition = this.getDoc();
    // let grafico_width = this.csvProvider.getWidth();
    this.pdfObj = pdfMake.createPdf(docDefinition);
  
    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });
 
        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }


  public montarRegional()
  {
    return new Promise ((resolve) => {
      for(let nome in this.supervisor_estadual){
        let aterCabecalho = {
          os:"REGIONAL",
          cpf:"NÃO INFORMADO",
          entrevistador:nome,
          data:"NÃO INFORMADO",
          uf:"NÃO INFORMADO"
        };
        let temp = new Ater(aterCabecalho);
        this.array_regional.push(temp);
      }
      let index;
      let flag_check_nome;
      var self = this;
      for(let elemento of self.array_os){
        flag_check_nome = false;
        let nome_local = elemento.entrevistador;
        for(let nome in this.supervisor_estadual){
          for(let agente of this.supervisor_estadual[nome].nomes)
          {
            if(agente.toUpperCase() == nome_local.toUpperCase())
            {
              if(nome == "Edmar de Oliveira e Silva")
              {
                console.log(nome_local);
              }
              index = this.supervisor_estadual[nome].index;
              flag_check_nome = true;
              let Q14 = {
                bovinocultura: elemento.Q14.bovinocultura,
                apicultura: elemento.Q14.apicultura,
                caprinocultura: elemento.Q14.caprinocultura,
                ovinocultura: elemento.Q14.ovinocultura,
                suinocultura: elemento.Q14.suinocultura,
                avicultura: elemento.Q14.avicultura,
                fruticultura: elemento.Q14.fruticultura,
                hortalicas: elemento.Q14.hortalicas,
                mandioca: elemento.Q14.mandioca,
                milho: elemento.Q14.milho,
                feijao: elemento.Q14.feijao,
                extrativismo: elemento.Q14.extrativismo,
                ensilagem: elemento.Q14.ensilagem,
                pesca: elemento.Q14.pesca,
                artesanato: elemento.Q14.artesanato,
                beneficiamento: elemento.Q14.beneficiamento,
                outros: elemento.Q14.outros,
              };
        
              let Q18 = {
                nova_forma_plantar: elemento.Q18.nova_forma_plantar,
                maquinas_agricolas: elemento.Q18.maquinas_agricolas,
                novo_animal: elemento.Q18.novo_animal,
                nova_forma_alimentacao_animal: elemento.Q18.nova_forma_alimentacao_animal,
                alteracao_manejo: elemento.Q18.alteracao_manejo,
                uso_irrigacao: elemento.Q18.uso_irrigacao,
                cobertura_solo: elemento.Q18.cobertura_solo,
                agroecologia: elemento.Q18.agroecologia,
                outros: elemento.Q18.outros,
              };
        
              let Q19 = {
                armazenamento: elemento.Q19.armazenamento,
                transporte: elemento.Q19.transporte,
                processamento_alimentos: elemento.Q19.processamento_alimentos,
                embalagem: elemento.Q19.embalagem,
                canal_comercializacao: elemento.Q19.canal_comercializacao,
                outros: elemento.Q19.outros,
              };
        
              let Q21 = {
                resposta: elemento.Q21
              };
        
              let Q22 = {
                resposta: elemento.Q22
              };
        
              let Q23 = {
                resposta: elemento.Q23
              };
        
              let Q31 = {
                resposta: elemento.Q31
              };
        
              let Q32 = {
                resposta: elemento.Q32
              };
        
              let Q34 = {
                resposta: elemento.Q34
              };
        
              let Q35 = {
                resposta: elemento.Q35
              };
        
              let Q36 = {
                resposta: elemento.Q36
              };
        
              let Q38 = {
                resposta: elemento.Q38
              };
              this.array_regional[index].addRegional(Q14, Q18, Q19, Q21, Q22, Q23, Q31, Q32, Q34, Q35, Q36, Q38);
              break;
            }
          }
          if(flag_check_nome) break;
        }
      }
      console.log(this.array_regional);
      resolve();
    });
  }

  public montarNacionalParcial()
  {
    return new Promise ((resolve) => {
      for(let nome in this.supervisor_nacional){
        let aterCabecalho = {
          os:"NACIONAL",
          cpf:"NÃO INFORMADO",
          entrevistador:nome,
          data:"NÃO INFORMADO",
          uf:"NÃO INFORMADO"
        };
        let temp = new Ater(aterCabecalho);
        this.array_nacional.push(temp);
      }
      let index;
      let flag_check_nome;
      var self = this;
      for(let elemento of self.array_regional){
        flag_check_nome = false;
        let nome_local = elemento.entrevistador;
        for(let nome in this.supervisor_nacional){
          for(let agente of this.supervisor_nacional[nome].nomes)
          {
            if(agente.toUpperCase() == nome_local.toUpperCase())
            {
              if(nome == "Edmar de Oliveira e Silva")
              {
                console.log(nome_local);
              }
              index = this.supervisor_nacional[nome].index;
              flag_check_nome = true;
              let Q14 = {
                bovinocultura: elemento.Q14.bovinocultura,
                apicultura: elemento.Q14.apicultura,
                caprinocultura: elemento.Q14.caprinocultura,
                ovinocultura: elemento.Q14.ovinocultura,
                suinocultura: elemento.Q14.suinocultura,
                avicultura: elemento.Q14.avicultura,
                fruticultura: elemento.Q14.fruticultura,
                hortalicas: elemento.Q14.hortalicas,
                mandioca: elemento.Q14.mandioca,
                milho: elemento.Q14.milho,
                feijao: elemento.Q14.feijao,
                extrativismo: elemento.Q14.extrativismo,
                ensilagem: elemento.Q14.ensilagem,
                pesca: elemento.Q14.pesca,
                artesanato: elemento.Q14.artesanato,
                beneficiamento: elemento.Q14.beneficiamento,
                outros: elemento.Q14.outros,
              };
        
              let Q18 = {
                nova_forma_plantar: elemento.Q18.nova_forma_plantar,
                maquinas_agricolas: elemento.Q18.maquinas_agricolas,
                novo_animal: elemento.Q18.novo_animal,
                nova_forma_alimentacao_animal: elemento.Q18.nova_forma_alimentacao_animal,
                alteracao_manejo: elemento.Q18.alteracao_manejo,
                uso_irrigacao: elemento.Q18.uso_irrigacao,
                cobertura_solo: elemento.Q18.cobertura_solo,
                agroecologia: elemento.Q18.agroecologia,
                outros: elemento.Q18.outros,
              };
        
              let Q19 = {
                armazenamento: elemento.Q19.armazenamento,
                transporte: elemento.Q19.transporte,
                processamento_alimentos: elemento.Q19.processamento_alimentos,
                embalagem: elemento.Q19.embalagem,
                canal_comercializacao: elemento.Q19.canal_comercializacao,
                outros: elemento.Q19.outros,
              };
        
              let Q21 = {
                resposta: elemento.Q21
              };
        
              let Q22 = {
                resposta: elemento.Q22
              };
        
              let Q23 = {
                resposta: elemento.Q23
              };
        
              let Q31 = {
                resposta: elemento.Q31
              };
        
              let Q32 = {
                resposta: elemento.Q32
              };
        
              let Q34 = {
                resposta: elemento.Q34
              };
        
              let Q35 = {
                resposta: elemento.Q35
              };
        
              let Q36 = {
                resposta: elemento.Q36
              };
        
              let Q38 = {
                resposta: elemento.Q38
              };
              this.array_nacional[index].addRegional(Q14, Q18, Q19, Q21, Q22, Q23, Q31, Q32, Q34, Q35, Q36, Q38);
              break;
            }
          }
          if(flag_check_nome) break;
        }
      }
      console.log(this.array_nacional);
      resolve();
    });
  }

  public montarNacionalTotal()
  {
    return new Promise ((resolve) => {
      for(let nome of this.supervisor_nacional2){
        let aterCabecalho = {
          os:"NACIONAL",
          cpf:"NÃO INFORMADO",
          entrevistador:nome,
          data:"NÃO INFORMADO",
          uf:"NÃO INFORMADO"
        };
        let temp = new Ater(aterCabecalho);
        this.array_nacional.push(temp);
      }
      let index;
      let flag_check_nome;
      var self = this;
      for(let elemento of self.array_regional){
        // flag_check_nome = false;
        // let nome_local = elemento.entrevistador;
        for(let elemento of this.array_regional){
          for(let nacional of this.array_nacional)
          {
            
              let Q14 = {
                bovinocultura: elemento.Q14.bovinocultura,
                apicultura: elemento.Q14.apicultura,
                caprinocultura: elemento.Q14.caprinocultura,
                ovinocultura: elemento.Q14.ovinocultura,
                suinocultura: elemento.Q14.suinocultura,
                avicultura: elemento.Q14.avicultura,
                fruticultura: elemento.Q14.fruticultura,
                hortalicas: elemento.Q14.hortalicas,
                mandioca: elemento.Q14.mandioca,
                milho: elemento.Q14.milho,
                feijao: elemento.Q14.feijao,
                extrativismo: elemento.Q14.extrativismo,
                ensilagem: elemento.Q14.ensilagem,
                pesca: elemento.Q14.pesca,
                artesanato: elemento.Q14.artesanato,
                beneficiamento: elemento.Q14.beneficiamento,
                outros: elemento.Q14.outros,
              };
        
              let Q18 = {
                nova_forma_plantar: elemento.Q18.nova_forma_plantar,
                maquinas_agricolas: elemento.Q18.maquinas_agricolas,
                novo_animal: elemento.Q18.novo_animal,
                nova_forma_alimentacao_animal: elemento.Q18.nova_forma_alimentacao_animal,
                alteracao_manejo: elemento.Q18.alteracao_manejo,
                uso_irrigacao: elemento.Q18.uso_irrigacao,
                cobertura_solo: elemento.Q18.cobertura_solo,
                agroecologia: elemento.Q18.agroecologia,
                outros: elemento.Q18.outros,
              };
        
              let Q19 = {
                armazenamento: elemento.Q19.armazenamento,
                transporte: elemento.Q19.transporte,
                processamento_alimentos: elemento.Q19.processamento_alimentos,
                embalagem: elemento.Q19.embalagem,
                canal_comercializacao: elemento.Q19.canal_comercializacao,
                outros: elemento.Q19.outros,
              };
        
              let Q21 = {
                resposta: elemento.Q21
              };
        
              let Q22 = {
                resposta: elemento.Q22
              };
        
              let Q23 = {
                resposta: elemento.Q23
              };
        
              let Q31 = {
                resposta: elemento.Q31
              };
        
              let Q32 = {
                resposta: elemento.Q32
              };
        
              let Q34 = {
                resposta: elemento.Q34
              };
        
              let Q35 = {
                resposta: elemento.Q35
              };
        
              let Q36 = {
                resposta: elemento.Q36
              };
        
              let Q38 = {
                resposta: elemento.Q38
              };
              nacional.addRegional(Q14, Q18, Q19, Q21, Q22, Q23, Q31, Q32, Q34, Q35, Q36, Q38);
              
            }
          }
      }
      console.log(this.array_nacional);
      resolve();
    });
  }

  public montarPDF2(itemAtual:number, nivel:string, array_data)
  {
    var self = this;
    if(itemAtual < array_data.length)
    // if(itemAtual == 0)
     {
      let row = array_data[itemAtual];
        let cabecalho = `RELATÓRIO NÍVEL ${nivel} - Questionário Inovação e ATER 
        (${row.entrevistador} OS: ${row.os})`;
        // let cabecalho = `RELATÓRIO NÍVEL ESTADUAL (${row.info_os.nome_entrevistador}`;
        let blocoA = `
          Q01. Quais as principais atividades produtivas realizadas pelos agricultores?  
        `;
        let blocoB = `
          Q02. Nos últimos 3 anos, houve alguma mudança importante na forma de realizar as atividades produtivas?   
        `;
        let blocoC = `
          Q03. Nos últimos 3 anos, as famílias adotaram alguma mudança importante na forma de “pós-produção/pós-colheita”?   
        `;
        let blocoD = `
          Q04. Nos últimos 3 anos, quantos entrevistados desistiram de iniciar uma atividade? 
        `;
        let blocoE = `
          Q05. Nos últimos 3 anos, quantos receberam assistência técnica?   
        `;
        let blocoF = `
          Q06. Quantos entrevistados desejam mudar a principal atividade econômica da propriedade?   
        `;
        let blocoG = `
          Q07. Quantos são beneficiários do Bolsa Família?  
        `;
        // let blocoH = `
        //   Q32. Quantos conhecem o Projeto Dom Hélder Câmera?  
        // `;
        let blocoI = `
          Q08. Quantos participam de algum dos Projetos do FIDA?
        `;
        let blocoJ = `
          Q09. Quantos participaram de alguma atividade do Projeto Dom Hélder Câmara entre os anos de 2001 e 2015?  
        `;
        let blocoK = `
          Q11. Quantos participam de alguma instância política/representativa? (Sindicatos, Cooperativas, Associações, Movimentos Sociais, Colegiados, Conselhos, Partidos Políticos, e etc)   
        `;
        let blocoL = `
          Q12. Quantos dos entrevistados já acessaram o benefício de Assistência Técnica e Extensão Rural – ATER?  
        `;
        if(row.entrevistador.toUpperCase() == "JÚLIA" || row.entrevistador.toUpperCase() == "LUIS ANTÔNIO DA SILVA SOARES")
        {
          self.plotarGrafico(itemAtual, array_data).then(() => { 
            let resposta18 = this.array_respostas[0];
            let resposta19 = this.array_respostas[1]
            self.conteudo.push(
              { text: cabecalho, style: 'header', alignment: 'center'  },
              { text: blocoA, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[0], width: 550, alignment: 'center'  },
              { text: blocoB, style: 'story', alignment: 'left'},
              { text: resposta18, style: 'story', alignment: 'left'},
              // { image:this.listaGraficos[1], width: 550, alignment: 'center',   },
              { text: blocoC, style: 'story', alignment: 'left',},
              { text: resposta19, style: 'story', alignment: 'left'},
              // { image:this.listaGraficos[2], width: 550, alignment: 'center' ,pageBreak: 'after'     },
              { text: blocoD, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[4], width: 600, alignment: 'center',  },
              { text: blocoE, style: 'story', alignment: 'left', },
              { image:this.listaGraficos[5], width: 600, alignment: 'center',  },
              { text: blocoF, style: 'story', alignment: 'left',},
              { image:this.listaGraficos[6], width: 600, alignment: 'center' ,},
              { text: blocoG, style: 'story', alignment: 'left', },
              { image:this.listaGraficos[7], width: 600, alignment: 'center'   },
              // { text: blocoH, style: 'story', alignment: 'left'},
              // { image:this.listaGraficos[7], width: 600, alignment: 'center',  },
              { text: blocoI, style: 'story', alignment: 'left',},
              { image:this.listaGraficos[9], width: 600, alignment: 'center' ,},
              { text: blocoJ, style: 'story', alignment: 'left',},
              { image:this.listaGraficos[10], width: 600, alignment: 'center',   },
              { text: blocoK, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[11], width: 600, alignment: 'center',},
              { text: blocoL, style: 'story', alignment: 'left',},
              { image:this.listaGraficos[12], width: 600, alignment: 'center',  pageBreak: 'after'},
              // { text:'', },
            );
              self.listaGraficos = [];
              itemAtual++;
              self.montarPDF2(itemAtual, nivel, array_data);
          });
        }
        else
        {
          self.plotarGrafico(itemAtual, array_data).then(() => { 
          let resposta18 = this.array_respostas[0];
          let resposta19 = this.array_respostas[1]
          self.conteudo.push(
            { text: cabecalho, style: 'header', alignment: 'center'  },
            { text: blocoA, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[0], width: 550, alignment: 'center'  },
            { text: blocoB, style: 'story', alignment: 'left'},
            { text: resposta18, style: 'story', alignment: 'left'},
            // { image:this.listaGraficos[1], width: 550, alignment: 'center',   },
            { text: blocoC, style: 'story', alignment: 'left',},
            { text: resposta19, style: 'story', alignment: 'left'},
            // { image:this.listaGraficos[2], width: 550, alignment: 'center' ,pageBreak: 'after'     },
            { text: blocoD, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[3], width: 600, alignment: 'center',  },
            { text: blocoE, style: 'story', alignment: 'left', },
            { image:this.listaGraficos[4], width: 600, alignment: 'center',  },
            { text: blocoF, style: 'story', alignment: 'left',},
            { image:this.listaGraficos[5], width: 600, alignment: 'center' ,},
            { text: blocoG, style: 'story', alignment: 'left', },
            { image:this.listaGraficos[6], width: 600, alignment: 'center'   },
            // { text: blocoH, style: 'story', alignment: 'left'},
            // { image:this.listaGraficos[7], width: 600, alignment: 'center',  },
            { text: blocoI, style: 'story', alignment: 'left',},
            { image:this.listaGraficos[8], width: 600, alignment: 'center' ,},
            { text: blocoJ, style: 'story', alignment: 'left',},
            { image:this.listaGraficos[9], width: 600, alignment: 'center',   },
            { text: blocoK, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[10], width: 600, alignment: 'center',},
            { text: blocoL, style: 'story', alignment: 'left',},
            { image:this.listaGraficos[11], width: 600, alignment: 'center',  pageBreak: 'after'},
            // { text:'', },
          );
            self.listaGraficos = [];
            itemAtual++;
            self.montarPDF2(itemAtual, "REGIONAL", array_data);
        });
        }
        
     }
     else
     {
       this.montarDocDefinition();
     }

     
  }
}

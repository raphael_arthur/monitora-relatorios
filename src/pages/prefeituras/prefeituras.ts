import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform  } from 'ionic-angular';
import { Http } from '@angular/http';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { GraficosProvider } from '../../providers/graficos/graficos';
import { MontarCsvProvider } from '../../providers/montar-csv/montar-csv';

import { Printer, PrintOptions } from '@ionic-native/printer';

import { Prefeitura } from '../../models/modelo_prefeitura';
import * as papa from 'papaparse';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
/**
 * Generated class for the PrefeiturasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prefeituras',
  templateUrl: 'prefeituras.html',
})
export class PrefeiturasPage {

  pdfObj = null;
  csvData: any[] = [];
  headerRow: any[] = [];

  conteudo:any = [];
  array_os: any;
  listaGraficos: any;
  docDefinition: { content: any[]; styles: { header: { fontSize: number; bold: boolean; }; subheader: { fontSize: number; bold: boolean; margin: number[]; }; story: { italic: boolean; alignment: string; width: string; }; }; };
  //+++++++++++++++++++++++++++++++++++++++++++++++++
  //============CONSTANTES CSV========================
  readonly entrevistador = 0; readonly uf = 1;  readonly C = 2;  readonly D = 3;  readonly E = 4;  readonly F = 5;  readonly G = 6;  readonly H = 7;
  readonly I = 8;             readonly J = 9;   readonly K = 10; readonly L = 11; readonly M = 12; readonly N = 13; readonly O = 14; readonly P = 15;
  readonly Q = 16;            readonly R = 17;  readonly S = 18; readonly T = 19; readonly U = 20; readonly V = 21; readonly W = 22; readonly X = 23;
  readonly Y = 24;            readonly Z = 25;  readonly AA = 26;readonly AB = 27;readonly AC = 28;readonly AD = 29;readonly AE = 30;readonly AF = 31;
  readonly AG = 32;           readonly AH = 33; readonly AI = 34;readonly AJ = 35;readonly AK = 36;readonly AL = 37;readonly AM = 38;readonly AN = 39;
  array_regional: any[];
  supervisor_estadual:any;
  supervisor_nacional2: string[];
  array_nacional: any[];
  supervisor_nacional: {
  "Júlia": { //AL CE SE PI
    index: number; nomes: string[];
  }; "LUANA BEATRIZ DE JESUS VELOSO FREITAS": { //MG,ES,BA
    index: number; nomes: string[];
  }; "Gabriela": { //PE,PB,MA,RN
    index: number; nomes: string[];
  };
  };

  constructor(
    public csvProvider:MontarCsvProvider, 
    private printer: Printer, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private http: Http, 
    private plt: Platform, 
    private file: File, 
    private fileOpener: FileOpener, 
    public graficos:GraficosProvider
  ){
    this.array_os = [];
    this.array_regional = [];
    this.supervisor_estadual = {
      "José Cleto de Araújo Alves":{
        index:0,
        nomes:["Cinthia Guimarães de Oliveira", 
        "Guilherme Belmonte",
        "John Carlos da Silva Pinto",
        "Luciano Flamorion da Cunha Moreira",
        "Wellington Bezerra Soares",
        "Ivanilson Ferreira da Silva"],
      },
      "Bruno Xavier Gomes": {
        index:1,
        nomes:[
        "Antônio Carlos Cerqueira Borges",
        "Antonio Milton de Oliveira Silva",//APENAS 1 COLETA, TUDO NÃO OU NÃO INFORMADO
        "Geraldo Sérgio Silva Ferreira",
        "Hamilton Almeida Torres",
        "Hugo Fernando Barbosa da Silva",
        "José Raimundo Oliveira dos Santos",
        "Joseleine Mendes Barbosa Souza",
        "Monike Keila Sousa de Oliveira",
        "Paulo Henrique Carneiro de Oliveira",
        "Wellington dos Santos Dourado",
        "Antonio Carlos Cerqueira Borges"
      ]},
      "Paulo Matos Barreto":{
        index:2,
        nomes:[
        "Antônio Alberto Benevides S. Júnior",
        "Antônio Minelvino de Souza Neto" ,
        "Arnóbio Rodrigues da Silva" ,
        "Francisco das Chagas Martins Bezerra" ,
        "Johnathan Emanuel Pinto Bessa" ,
        "José Antônio Martins da Silva" ,
        "José Junior Gonçalves Noronha",
        "Manoel Vieira de Carvalho Filho" ,
        "Ramille Gonçalves de Souza" ,
        "Rodrigo de Andrade Rodrigues" ,
        "Yvon Saymon Canuto Vieira" ,
        "Antonio Alberto Benevides S. Júnior",
        "Ramille Gonçalves de Sousa",
        "Marciel da Silva Freire"
      ]},
      "Diego Capucho Cezana":{
        index:3,
        nomes:[
        "Daniele Batista Caetano",
        "Evaldo Nalli Ramos",
        "Sandra Madalena Valentim de Souza",
        'SANDRO SOARES TAVARES',
        'ADAILTON DUARTE LIMA',
        'ALEXSANDRO PEREIRA PEDROSA',
        'CELY MOURA AMARAL',
        'PAULO ROGÉRIO DE MEDEIROS SILVA',
        'SIDE CLAUDIO COSTA BARROS',
        
      ]},
      "Sandro Soares Tavares":{
        index:4,
        nomes:[
        'Adailton Duarte Lima',
        'Alexsandro Pereira Pedrosa',
        'Cely Moura Amaral',
        'Paulo Rogério de Medeiros Silva',
        'Side Claudio Costa Barros',
        "Silde Soares Tavares"
      ]},
      "Dinovan Queiroz de Almeida":{
        index:5,
        nomes:[
        'Adriana Viana Chagas',
        'Carlos Ruas Reis',
        'Fábio Ernani de Oliveira',
        'Fábio Luiz Becker',
        'Geraldo Aparecido Miguel Soares',
        'Hercules Vandy Durães da Fonseca',
        'João Eder Oliveira Almeida',
        'José Nilton Reis Santana',
        'Roselmiro Guimarães Entreportes',
        'Saulo Souza Matos',
        "Ezequiel Alves Martins"
      ]},
      "Julio Breno Vieira Lemos":{
        index:6,
        nomes:[
        'Adailson Bernardo dos Santos',
        'Bilac Soares de Oliveira',
        'Francisco de Luna Alves',
        'Leonel da Costa Azevedo',
        'Paulo Sergio Lins Dantas',
        'Robério Macêdo de Oliveira',
        'Victor Hugo Benevides Trigueiro Targino',
        'Lindinalva Oliveira de Paulo',
        'Paulo Lacerda de Oliveira',
        'Pablo Antonio de Sousa Monteiro',
        'Ana Palhano Freire Neta',
        'Jorismar dos Reis Fernandes'
      ]},
      "Edmar de Oliveira e Silva":{
        index:7,
        nomes:[
        'Antônio Florêncio Barros Medrado',
        'Eguimagno Rodrigues de Morais' ,
        'Esmar Esmeraldo Santos' ,
        'Francilene Pereira do Nascimento',
        'José Acevedo Alves Junior',
        'Júlio Cavalcante Lacerda Neto (Julinho)',
        'Kacia Amando da Silva' ,
        'Marcus Andre Pereira de Moura' ,
        'Rivanildo Adones dos Santos' ,
        'Wandebergue Luiz da Silva',
        'Luiz Canavello Neto'
      ]},
      "Bruno Fonseca Guerra":{
        index:8,
        nomes:[
        'Aemerson Rodrigues de Carvalho' ,
        'Ayrton Feitosa Santana' ,
        'Cicero Jose de Sousa',
        'Edilson de Souza Carvalho' ,
        'Hudson Maia de Carvalho',
        'Irineu Antonio da Costa' ,
        'Joilson Lustosa Silva Santana' ,
        'José Emilton Silva de Souza' ,
        'Wagner Sabino de Sousa Silva' ,
      ]},
      "Raimundo Thiago F. Moura de Araújo":{
        index:9,
        nomes:[
        'Eclecio Fernandes da Cunha' ,
        'Edjane Marinho da Costa' ,
        'Everton Silva Santos' ,
        'Fernanda Cinthia Fernandes Dantas',
        'Jonathas de Albuquerque Monteiro Bezerra',
        'José Jubenick Pereira da Silva' ,
        'José Leonilson Jácome Bezerra' ,
        'Lylyam Bibiana de Oliveira Fernandes' ,
        'Suelda Varela Caldas'
      ]},
      "Neydmar Loureiro dos Reis Lima":{
        index:10,
        nomes:[
        'Berenice Damazio Santos',
        'Reginaldo Pereira dos Santos',
        'Izaque Vieira dos Santos',
        'Wesley Santos Aragão',
        'Ana Maria Moura'
      ]}
    }
    this.supervisor_nacional2 = [
      'LUIS ANTÔNIO DA SILVA SOARES',
    ];
    this.array_nacional = [];

    this.supervisor_nacional = {
      "Júlia":{//AL CE SE PI
        index:0,
        nomes:["JOSÉ CLETO DE ARAÚJO ALVES", //AL 
        "PAULO MATOS BARRETO", //CE
        "NEYDMAR LOUREIRO DOS REIS LIMA", //SE
        "BRUNO FONSECA GUERRA", //PI
        ],
      },
      "LUANA BEATRIZ DE JESUS VELOSO FREITAS":{ //MG,ES,BA
        index:1,
        nomes:["DINOVAN QUEIROZ DE ALMEIDA",//MG 
        "DIEGO CAPUCHO CEZANA", //ES
        "BRUNO XAVIER GOMES", //BA
        ],
      },
      "Gabriela":{ //PE,PB,MA,RN
        index:2,
        nomes:["EDMAR DE OLIVEIRA E SILVA", //PE 
        "JULIO BRENO VIEIRA LEMOS", //PB
        "SANDRO SOARES TAVARES", //MA
        "RAIMUNDO THIAGO F. MOURA DE ARAÚJO",//RN
        ],
      },
    }
    this.readCsvData();
  }

  public readCsvData() {
    return new Promise ((resolve) => {
      this.http.get('assets/prefeituras.csv')
      .subscribe(
      data => this.extractData(data),
      err => this.handleError(err)
      );
      resolve();
    });
  }

  private handleError(err) {
    //console.log('something went wrong: ', err);
  }
 
  trackByFn(index: any, item: any) {
    return index;
  }

  private extractData(res) {
    let csvData = res['_body'] || '';
    let parsedData = papa.parse(csvData, {delimiter:";"}).data;
 
    this.headerRow = parsedData[1];
 
    parsedData.splice(0, 2);
    this.csvData = parsedData;
    // //console.log(this.headerRow);
    // //console.log(this.csvData);
    var contador = 0;
    for(let i=0; i < this.csvData.length; i++)
    {
      let prefeituraCabecalho = {
        entrevistador:this.csvData[i][this.entrevistador],
        uf:this.csvData[i][this.uf],
        os:this.csvData[i][this.AN],
      };

      let Q01 = {
        saude: this.csvData[i][this.C],
        educacao: this.csvData[i][this.D],
        pobreza: this.csvData[i][this.E],
        emprego: this.csvData[i][this.F],
        seguranca_alimentar:this.csvData[i][this.G],
        habitacao: this.csvData[i][this.H],
        transportes: this.csvData[i][this.I],
        seguranca: this.csvData[i][this.J],
        previdencia: this.csvData[i][this.K],
        ater: this.csvData[i][this.L],
      }

      let Q02 = {
        bolsa_familia: this.csvData[i][this.M],
        luz_no_campo: this.csvData[i][this.N],
        luz_para_todos: this.csvData[i][this.O],
        cisterna_1: this.csvData[i][this.P],
        cisterna_2: this.csvData[i][this.Q],
        extensao_rural: this.csvData[i][this.R],
        financiamento_agricola: this.csvData[i][this.S],
        pronaf: this.csvData[i][this.T],
        paa: this.csvData[i][this.U],
        pnae: this.csvData[i][this.V],
    };

    let Q04 = {
      sim: this.csvData[i][this.W],
      nao: this.csvData[i][this.X],
    };

    let Q05 = {
        sim: this.csvData[i][this.Y],
        nao: this.csvData[i][this.Z],
    };

    let Q06 = {
        extensao_rural: this.csvData[i][this.AA],
        prefeitura: this.csvData[i][this.AB],
        estado: this.csvData[i][this.AC],
        ongs: this.csvData[i][this.AD],
        paa: this.csvData[i][this.AE],
        pnae: this.csvData[i][this.AG],
        garantia_safra: this.csvData[i][this.AI],
        feiras_livres: this.csvData[i][this.AK],
        cursos: this.csvData[i][this.AL],
        estradas_rurais: this.csvData[i][this.AM],
    };


      if(i>0)
      {
        contador++;
        if((this.csvData[i][this.entrevistador] != this.csvData[i-1][this.entrevistador])|| (this.csvData[i][this.AN] != this.csvData[i-1][this.AN]))
        {
          let temp = new Prefeitura(prefeituraCabecalho);
          temp.addInfoEntrevista(Q01, Q02, Q04, Q05, Q06 );
          this.array_os.push(temp);
        }
        else{
          let ult_index = this.array_os.length-1;
          // //console.log(this.array_os[ult_index]);
          this.array_os[ult_index].addInfoEntrevista(Q01, Q02, Q04, Q05, Q06 );
        }
      }
      else if(i == 0)
      {
        contador++;
        let temp = new Prefeitura(prefeituraCabecalho);
        temp.addInfoEntrevista(Q01, Q02, Q04, Q05, Q06 );
        this.array_os.push(temp);
      }      
    }

    console.log(this.array_os);
    // this.montarPDF(0);
    this.montarRegional().then(() =>{
      this.montarPDF2(0, "REGIONAL", this.array_regional);
      this.montarNacionalParcial().then(() =>{
        this.montarPDF2(0, "NACIONAL", this.array_nacional);
      });
    });
  }

  public montarRegional()
  {
    return new Promise ((resolve) => {
      for(let nome in this.supervisor_estadual){
        let aterCabecalho = {
          os:"REGIONAL",
          entrevistador:nome,
          uf:"NÃO INFORMADO"
        };
        let temp = new Prefeitura(aterCabecalho);
        this.array_regional.push(temp);
      }
      let index;
      let flag_check_nome;
      var self = this;
      for(let elemento of self.array_os){
        flag_check_nome = false;
        let nome_local = elemento.entrevistador;
        for(let nome in this.supervisor_estadual){
          for(let agente of this.supervisor_estadual[nome].nomes)
          {
            if(agente.toUpperCase() == nome_local.toUpperCase())
            {
              if(nome == "Edmar de Oliveira e Silva")
              {
                console.log(nome_local);
              }
              index = this.supervisor_estadual[nome].index;
              flag_check_nome = true;
              let Q01 = {
                saude: elemento.Q01.saude,
                educacao: elemento.Q01.educacao,
                pobreza: elemento.Q01.pobreza,
                emprego: elemento.Q01.emprego,
                seguranca_alimentar:elemento.Q01.seguranca_alimentar,
                habitacao: elemento.Q01.habitacao,
                transportes: elemento.Q01.transportes,
                seguranca: elemento.Q01.seguranca,
                previdencia: elemento.Q01.previdencia,
                ater: elemento.Q01.ater,
              }
        
              let Q02 = {
                bolsa_familia: elemento.Q02.bolsa_familia,
                luz_no_campo: elemento.Q02.luz_no_campo,
                luz_para_todos: elemento.Q02.luz_para_todos,
                cisterna_1: elemento.Q02.cisterna_1,
                cisterna_2: elemento.Q02.cisterna_2,
                extensao_rural: elemento.Q02.extensao_rural,
                financiamento_agricola: elemento.Q02.financiamento_agricola,
                pronaf: elemento.Q02.pronaf,
                paa: elemento.Q02.paa,
                pnae: elemento.Q02.pnae,
            };
        
              let Q04 = {
                sim: elemento.Q04.sim,
                nao: elemento.Q04.nao,
              };
          
              let Q05 = {
                  sim: elemento.Q05.sim,
                  nao: elemento.Q05.nao,
              };
          
              let Q06 = {
                  extensao_rural: elemento.Q06.extensao_rural,
                  prefeitura: elemento.Q06.prefeitura,
                  estado: elemento.Q06.estado,
                  ongs: elemento.Q06.ongs,
                  paa: elemento.Q06.paa,
                  pnae: elemento.Q06.pnae,
                  garantia_safra: elemento.Q06.garantia_safra,
                  feiras_livres: elemento.Q06.feiras_livres,
                  cursos: elemento.Q06.cursos,
                  estradas_rurais: elemento.Q06.estradas_rurais,
              };

              this.array_regional[index].addRegional(Q01, Q02, Q04, Q05, Q06);
              break;
            }
          }
          if(flag_check_nome) break;
        }
      }
      console.log(this.array_regional);
      resolve();
    });
  }

  public montarNacionalParcial()
  {
    return new Promise ((resolve) => {
      for(let nome in this.supervisor_nacional){
        let aterCabecalho = {
          os:"NACIONAL",
          entrevistador:nome,
          uf:"NÃO INFORMADO"
        };
        let temp = new Prefeitura(aterCabecalho);
        this.array_nacional.push(temp);
      }
      let index;
      let flag_check_nome;
      var self = this;
      for(let elemento of self.array_regional){
        flag_check_nome = false;
        let nome_local = elemento.entrevistador;
        for(let nome in this.supervisor_nacional){
          for(let agente of this.supervisor_nacional[nome].nomes)
          {
            if(agente.toUpperCase() == nome_local.toUpperCase())
            {
              if(nome == "Edmar de Oliveira e Silva")
              {
                console.log(nome_local);
              }
              index = this.supervisor_nacional[nome].index;
              flag_check_nome = true;
              let Q01 = {
                saude: elemento.Q01.saude,
                educacao: elemento.Q01.educacao,
                pobreza: elemento.Q01.pobreza,
                emprego: elemento.Q01.emprego,
                seguranca_alimentar:elemento.Q01.seguranca_alimentar,
                habitacao: elemento.Q01.habitacao,
                transportes: elemento.Q01.transportes,
                seguranca: elemento.Q01.seguranca,
                previdencia: elemento.Q01.previdencia,
                ater: elemento.Q01.ater,
              }
        
              let Q02 = {
                bolsa_familia: elemento.Q02.bolsa_familia,
                luz_no_campo: elemento.Q02.luz_no_campo,
                luz_para_todos: elemento.Q02.luz_para_todos,
                cisterna_1: elemento.Q02.cisterna_1,
                cisterna_2: elemento.Q02.cisterna_2,
                extensao_rural: elemento.Q02.extensao_rural,
                financiamento_agricola: elemento.Q02.financiamento_agricola,
                pronaf: elemento.Q02.pronaf,
                paa: elemento.Q02.paa,
                pnae: elemento.Q02.pnae,
            };
        
              let Q04 = {
                sim: elemento.Q04.sim,
                nao: elemento.Q04.nao,
              };
          
              let Q05 = {
                  sim: elemento.Q05.sim,
                  nao: elemento.Q05.nao,
              };
          
              let Q06 = {
                  extensao_rural: elemento.Q06.extensao_rural,
                  prefeitura: elemento.Q06.prefeitura,
                  estado: elemento.Q06.estado,
                  ongs: elemento.Q06.ongs,
                  paa: elemento.Q06.paa,
                  pnae: elemento.Q06.pnae,
                  garantia_safra: elemento.Q06.garantia_safra,
                  feiras_livres: elemento.Q06.feiras_livres,
                  cursos: elemento.Q06.cursos,
                  estradas_rurais: elemento.Q06.estradas_rurais,
              };

              this.array_nacional[index].addRegional(Q01, Q02, Q04, Q05, Q06);
              break;
            }
          }
          if(flag_check_nome) break;
        }
      }
      console.log(this.array_nacional);
      resolve();
    });
  }

  public montarNacionalTotal()
  {
    return new Promise ((resolve) => {
      for(let nome of this.supervisor_nacional2){
        let aterCabecalho = {
          os:"NACIONAL",
          entrevistador:nome,
          uf:"NÃO INFORMADO"
        };
        let temp = new Prefeitura(aterCabecalho);
        this.array_nacional.push(temp);
      }
      let index;
      let flag_check_nome;
      var self = this;
      // for(let elemento of self.array_regional){
        // flag_check_nome = false;
        // let nome_local = elemento.entrevistador;
        for(let elemento of this.array_regional){
          for(let nacional of this.array_nacional)
          {
              let Q01 = {
                saude: elemento.Q01.saude,
                educacao: elemento.Q01.educacao,
                pobreza: elemento.Q01.pobreza,
                emprego: elemento.Q01.emprego,
                seguranca_alimentar:elemento.Q01.seguranca_alimentar,
                habitacao: elemento.Q01.habitacao,
                transportes: elemento.Q01.transportes,
                seguranca: elemento.Q01.seguranca,
                previdencia: elemento.Q01.previdencia,
                ater: elemento.Q01.ater,
              }
        
              let Q02 = {
                bolsa_familia: elemento.Q02.bolsa_familia,
                luz_no_campo: elemento.Q02.luz_no_campo,
                luz_para_todos: elemento.Q02.luz_para_todos,
                cisterna_1: elemento.Q02.cisterna_1,
                cisterna_2: elemento.Q02.cisterna_2,
                extensao_rural: elemento.Q02.extensao_rural,
                financiamento_agricola: elemento.Q02.financiamento_agricola,
                pronaf: elemento.Q02.pronaf,
                paa: elemento.Q02.paa,
                pnae: elemento.Q02.pnae,
            };
        
              let Q04 = {
                sim: elemento.Q04.sim,
                nao: elemento.Q04.nao,
              };
          
              let Q05 = {
                  sim: elemento.Q05.sim,
                  nao: elemento.Q05.nao,
              };
          
              let Q06 = {
                  extensao_rural: elemento.Q06.extensao_rural,
                  prefeitura: elemento.Q06.prefeitura,
                  estado: elemento.Q06.estado,
                  ongs: elemento.Q06.ongs,
                  paa: elemento.Q06.paa,
                  pnae: elemento.Q06.pnae,
                  garantia_safra: elemento.Q06.garantia_safra,
                  feiras_livres: elemento.Q06.feiras_livres,
                  cursos: elemento.Q06.cursos,
                  estradas_rurais: elemento.Q06.estradas_rurais,
              };

              nacional.addRegional(Q01, Q02, Q04, Q05, Q06);
            }
        }
      // }
      console.log(this.array_nacional);
      resolve();
    });
  }

  public montarPDF(itemAtual:number)
  {
    var self = this;
    if(itemAtual < self.array_os.length)
    // if(itemAtual == 0)
     {
      let row = self.array_os[itemAtual];
        let cabecalho = `RELATÓRIO NÍVEL LOCAL - PREFEITURAS (${row.entrevistador} OS: ${row.os})`;
        // let cabecalho = `RELATÓRIO NÍVEL ESTADUAL (${row.info_os.nome_entrevistador}`;
        let blocoQ1 = `
          Q01. Quais as áreas de maior carência para os agricultores do município?   
        `;
        let blocoQ2 = `
          Q02. Atualmente, quais Projetos e Políticas atendem os agricultores familiares?  
        `;
        let blocoQ4 = `
          Q04. Qual porcentagem dos entrevistados conhecem o Projeto Dom Hélder Câmara? 
        `;
        let blocoQ5 = `
          Q08. A prefeitura municipal realiza compras para a alimentação escolar (PNAE) de agricultores do município? 
        `;
        let blocoQ06 = `
          Q19. Quais as principais atividade desenvolvidas pela Prefeitura para benefício dos agricultores familiares? 
        `;
        
        self.plotarGrafico(itemAtual, self.array_os).then(() => { 
          console.log(self.listaGraficos);
          self.conteudo.push(
            { text: cabecalho, style: 'header', alignment: 'center'  },
            { text: blocoQ1, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[0], width: 550, alignment: 'center'  },
            { text: blocoQ2, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[1], width: 550, alignment: 'center' ,pageBreak:'after' },
            { text: blocoQ4, style: 'story', alignment: 'left',},
            { image:this.listaGraficos[2], width: 550, alignment: 'center' , },
            { text: blocoQ5, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[3], width: 600, alignment: 'center',  },
            { text: blocoQ06, style: 'story', alignment: 'left', },
            { image:this.listaGraficos[4], width: 600, alignment: 'center', pageBreak:'after' },
          );
            self.listaGraficos = [];
            itemAtual++;
            self.montarPDF(itemAtual);
        });
     }
     else
     {
       this.montarDocDefinition();
     }

     
  }

  public montarPDF2(itemAtual:number, nivel:string, array_data)
  {
    var self = this;
    if(itemAtual < array_data.length)
    // if(itemAtual == 0)
     {
      let row = array_data[itemAtual];
        let cabecalho = `RELATÓRIO NÍVEL ${nivel} - PREFEITURAS (${row.entrevistador} OS: ${row.os})`;
        // let cabecalho = `RELATÓRIO NÍVEL ESTADUAL (${row.info_os.nome_entrevistador}`;
        let blocoQ1 = `
          Q01. Quais as áreas de maior carência para os agricultores do município?   
        `;
        let blocoQ2 = `
          Q02. Atualmente, quais Projetos e Políticas atendem os agricultores familiares?  
        `;
        let blocoQ4 = `
          Q04. Qual porcentagem dos entrevistados conhecem o Projeto Dom Hélder Câmara? 
        `;
        let blocoQ5 = `
          Q08. A prefeitura municipal realiza compras para a alimentação escolar (PNAE) de agricultores do município? 
        `;
        let blocoQ06 = `
          Q19. Quais as principais atividade desenvolvidas pela Prefeitura para benefício dos agricultores familiares? 
        `;
        if(row.entrevistador.toUpperCase() == "JÚLIA" || row.entrevistador.toUpperCase() == "LUIS ANTÔNIO DA SILVA SOARES")
        {
          self.plotarGrafico(itemAtual, array_data).then(() => { 
            console.log(self.listaGraficos);
            self.conteudo.push(
              { text: cabecalho, style: 'header', alignment: 'center'  },
              { text: blocoQ1, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[1], width: 550, alignment: 'center'  },
              { text: blocoQ2, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[2], width: 550, alignment: 'center' ,pageBreak:'after' },
              { text: blocoQ4, style: 'story', alignment: 'left',},
              { image:this.listaGraficos[3], width: 550, alignment: 'center' , },
              { text: blocoQ5, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[4], width: 600, alignment: 'center',  },
              { text: blocoQ06, style: 'story', alignment: 'left', },
              { image:this.listaGraficos[5], width: 600, alignment: 'center', pageBreak:'after' },
            );
              self.listaGraficos = [];
              itemAtual++;
              self.montarPDF2(itemAtual, nivel, array_data);
          });
        }
        else
        {
          self.plotarGrafico(itemAtual, array_data).then(() => { 
            console.log(self.listaGraficos);
            self.conteudo.push(
              { text: cabecalho, style: 'header', alignment: 'center'  },
              { text: blocoQ1, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[0], width: 550, alignment: 'center'  },
              { text: blocoQ2, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[1], width: 550, alignment: 'center' ,pageBreak:'after' },
              { text: blocoQ4, style: 'story', alignment: 'left',},
              { image:this.listaGraficos[2], width: 550, alignment: 'center' , },
              { text: blocoQ5, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[3], width: 600, alignment: 'center',  },
              { text: blocoQ06, style: 'story', alignment: 'left', },
              { image:this.listaGraficos[4], width: 600, alignment: 'center', pageBreak:'after' },
            );
              self.listaGraficos = [];
              itemAtual++;
              self.montarPDF2(itemAtual, nivel, array_data);
          });
        }
     }
     else
     {
       this.montarDocDefinition();
     }

     
  }

  private plotarGrafico(itemAtual:number, array:any)
  {
    return new Promise ((resolve) => {
      var self = this;
      //-------------QUESTÃO 01-------------
       
      let Q01_length = [
        {
          tipo: "Saúde",
          valor:[//NI, ruim, regular, bom
            array[itemAtual].Q01.saude.filter(item => item === "").length/array[itemAtual].Q01.saude.length*100,
            array[itemAtual].Q01.saude.filter(item => (item == 1 || item == 2 || item == 3 || item == 4)).length/array[itemAtual].Q01.saude.length*100,
            array[itemAtual].Q01.saude.filter(item => (item == 5 || item == 6 || item == 7 || item == 8)).length/array[itemAtual].Q01.saude.length*100,
            array[itemAtual].Q01.saude.filter(item => (item == 9 || item == 10 || item == 11 || item == 12)).length/array[itemAtual].Q01.saude.length*100,
          ]
        },
        {
          tipo: "Educação",
          valor:[
            array[itemAtual].Q01.educacao.filter(item => item === "").length/array[itemAtual].Q01.educacao.length*100,
            array[itemAtual].Q01.educacao.filter(item => (item == 1 || item == 2 || item == 3 || item == 4)).length/array[itemAtual].Q01.educacao.length*100,
            array[itemAtual].Q01.educacao.filter(item => (item == 5 || item == 6 || item == 7 || item == 8)).length/array[itemAtual].Q01.educacao.length*100,
            array[itemAtual].Q01.educacao.filter(item => (item == 9 || item == 10 || item == 11 || item == 12)).length/array[itemAtual].Q01.educacao.length*100,
          ]
        },
        {
          tipo: "Pobreza",
          valor:[
            array[itemAtual].Q01.pobreza.filter(item => item === "").length/array[itemAtual].Q01.pobreza.length*100,
            array[itemAtual].Q01.pobreza.filter(item => (item == 1 || item == 2 || item == 3 || item == 4)).length/array[itemAtual].Q01.pobreza.length*100,
            array[itemAtual].Q01.pobreza.filter(item => (item == 5 || item == 6 || item == 7 || item == 8)).length/array[itemAtual].Q01.pobreza.length*100,
            array[itemAtual].Q01.pobreza.filter(item => (item == 9 || item == 10 || item == 11 || item == 12)).length/array[itemAtual].Q01.pobreza.length*100,
          ]
        },
        {
          tipo: "Emprego",
          valor:[
            array[itemAtual].Q01.emprego.filter(item => item === "").length/array[itemAtual].Q01.emprego.length*100,
            array[itemAtual].Q01.emprego.filter(item => (item == 1 || item == 2 || item == 3 || item == 4)).length/array[itemAtual].Q01.emprego.length*100,
            array[itemAtual].Q01.emprego.filter(item => (item == 5 || item == 6 || item == 7 || item == 8)).length/array[itemAtual].Q01.emprego.length*100,
            array[itemAtual].Q01.emprego.filter(item => (item == 9 || item == 10 || item == 11 || item == 12)).length/array[itemAtual].Q01.emprego.length*100,
          ]
        },
        {
          tipo: "Segurança alimentar",
          valor:[
            array[itemAtual].Q01.seguranca_alimentar.filter(item => item === "").length/array[itemAtual].Q01.seguranca_alimentar.length*100,
            array[itemAtual].Q01.seguranca_alimentar.filter(item => (item == 1 || item == 2 || item == 3 || item == 4)).length/array[itemAtual].Q01.seguranca_alimentar.length*100,
            array[itemAtual].Q01.seguranca_alimentar.filter(item => (item == 5 || item == 6 || item == 7 || item == 8)).length/array[itemAtual].Q01.seguranca_alimentar.length*100,
            array[itemAtual].Q01.seguranca_alimentar.filter(item => (item == 9 || item == 10 || item == 11 || item == 12)).length/array[itemAtual].Q01.seguranca_alimentar.length*100,
        
          ]
        },
        {
          tipo: "Habitação",
          valor:[
            array[itemAtual].Q01.habitacao.filter(item => item === "").length/array[itemAtual].Q01.habitacao.length*100,
            array[itemAtual].Q01.habitacao.filter(item => (item == 1 || item == 2 || item == 3 || item == 4)).length/array[itemAtual].Q01.habitacao.length*100,
            array[itemAtual].Q01.habitacao.filter(item => (item == 5 || item == 6 || item == 7 || item == 8)).length/array[itemAtual].Q01.habitacao.length*100,
            array[itemAtual].Q01.habitacao.filter(item => (item == 9 || item == 10 || item == 11 || item == 12)).length/array[itemAtual].Q01.habitacao.length*100,
          ]
        },
        {
          tipo: "Transportes",
          valor:[
            array[itemAtual].Q01.transportes.filter(item => item === "").length/array[itemAtual].Q01.transportes.length*100,
            array[itemAtual].Q01.transportes.filter(item => (item == 1 || item == 2 || item == 3 || item == 4)).length/array[itemAtual].Q01.transportes.length*100,
            array[itemAtual].Q01.transportes.filter(item => (item == 5 || item == 6 || item == 7 || item == 8)).length/array[itemAtual].Q01.transportes.length*100,
            array[itemAtual].Q01.transportes.filter(item => (item == 9 || item == 10 || item == 11 || item == 12)).length/array[itemAtual].Q01.transportes.length*100,
          ]
        },
        {
          tipo: "Segurança",
          valor:[
            array[itemAtual].Q01.seguranca.filter(item => item === "").length/array[itemAtual].Q01.seguranca.length*100,
            array[itemAtual].Q01.seguranca.filter(item => (item == 1 || item == 2 || item == 3 || item == 4)).length/array[itemAtual].Q01.seguranca.length*100,
            array[itemAtual].Q01.seguranca.filter(item => (item == 5 || item == 6 || item == 7 || item == 8)).length/array[itemAtual].Q01.seguranca.length*100,
            array[itemAtual].Q01.seguranca.filter(item => (item == 9 || item == 10 || item == 11 || item == 12)).length/array[itemAtual].Q01.seguranca.length*100,
          ]
        },
        {
          tipo: "Previdência social",
          valor:[
            array[itemAtual].Q01.previdencia.filter(item => item === "").length/array[itemAtual].Q01.previdencia.length*100,
            array[itemAtual].Q01.previdencia.filter(item => (item == 1 || item == 2 || item == 3 || item == 4)).length/array[itemAtual].Q01.previdencia.length*100,
            array[itemAtual].Q01.previdencia.filter(item => (item == 5 || item == 6 || item == 7 || item == 8)).length/array[itemAtual].Q01.previdencia.length*100,
            array[itemAtual].Q01.previdencia.filter(item => (item == 9 || item == 10 || item == 11 || item == 12)).length/array[itemAtual].Q01.previdencia.length*100,
  
          ],
        },
        {
          tipo: "Assistência Técnica e Extensão Rural - ATER",
          valor:[
            array[itemAtual].Q01.ater.filter(item => item === "").length/array[itemAtual].Q01.ater.length*100,
            array[itemAtual].Q01.ater.filter(item => (item == 1 || item == 2 || item == 3 || item == 4)).length/array[itemAtual].Q01.ater.length*100,
            array[itemAtual].Q01.ater.filter(item => (item == 5 || item == 6 || item == 7 || item == 8)).length/array[itemAtual].Q01.ater.length*100,
            array[itemAtual].Q01.ater.filter(item => (item == 9 || item == 10 || item == 11 || item == 12)).length/array[itemAtual].Q01.ater.length*100,
          ]
        },
      ];

    //   console.log(Q01_length);
    //   Q01_length = Q01_length.sort((a,b) => {
    //     if(a.valor[1] < b.valor[1]){
    //       return 1;
    //     }
    //     if(a.valor[1] > b.valor[1]){
    //       return -1;
    //     }
    //     return 0;
    // });
      let dataQ01_NI = Q01_length.map(a => (a.valor[0]));
      let dataQ01_ruim = Q01_length.map(a => (a.valor[1]));
      let dataQ01_regular = Q01_length.map(a => (a.valor[2]));
      let dataQ01_bom = Q01_length.map(a => (a.valor[3]));
      let labelsQ01 = Q01_length.map(a => (a.tipo));
      console.log(dataQ01_NI);
      console.log(labelsQ01);

      let dataGraficoBlocoQ01:any;
      dataGraficoBlocoQ01 = {
        div:"graficoBlocoQ01",
        titulo:"",
        subtitulo:"",
        xAxis: labelsQ01,
        yAxis: "",
        // eixoX: "dinamico conforme tabela TODO",
        // eixoY: "% de respostas SIM",
        // eixoX: [labelsQ31],
        series: [
        {
          name: 'Bom',
          data: dataQ01_bom,
        },
        {
          name: 'Regular',
          data: dataQ01_regular,
        },
        {
          name: 'Ruim',
          data: dataQ01_ruim,
        },
        {
          name: 'Não informado',
          data: dataQ01_NI,
        },
        ]
      };

      var graficoBlocoQ01 = this.graficos.plotarGraficoStackedBar(dataGraficoBlocoQ01);

    //------------------------------------------
    //-----------------Questão 02---------------
    let Q02_length  = [
      {tipo:'Bolsa família', valor: array[itemAtual].Q02.bolsa_familia.filter(item => item == "1").length/array[itemAtual].Q02.bolsa_familia.length*100,},
      {tipo:'Luz no campo', valor: array[itemAtual].Q02.luz_no_campo.filter(item => item == "1").length/array[itemAtual].Q02.luz_no_campo.length*100,},
      {tipo:'Luz para todos', valor: array[itemAtual].Q02.luz_para_todos.filter(item => item == "1").length/array[itemAtual].Q02.luz_para_todos.length*100,},
      {tipo:'Cisterna para consumo humano - 1ª Água', valor: array[itemAtual].Q02.cisterna_1.filter(item => item == "1").length/array[itemAtual].Q02.cisterna_1.length*100},
      {tipo:'Cisterna para consumo humano - 2ª Água', valor: array[itemAtual].Q02.cisterna_2.filter(item => item == "1").length/array[itemAtual].Q02.cisterna_2.length*100},
      {tipo:'Assistência técnica e extensão rural', valor: array[itemAtual].Q02.extensao_rural.filter(item => item == "1").length/array[itemAtual].Q02.extensao_rural.length*100},
      {tipo:'Financiamento Agrícola', valor: array[itemAtual].Q02.financiamento_agricola.filter(item => item == "1").length/array[itemAtual].Q02.financiamento_agricola.length*100},
      {tipo:'PRONAF', valor: array[itemAtual].Q02.pronaf.filter(item => item == "1").length/array[itemAtual].Q02.pronaf.length*100},
      {tipo:'PAA', valor: array[itemAtual].Q02.paa.filter(item => item == "1").length/array[itemAtual].Q02.paa.length*100},
      {tipo:'PNAE', valor: array[itemAtual].Q02.pnae.filter(item => item == "1").length/array[itemAtual].Q02.pnae.length*100},
      
    ];  
  
    Q02_length = Q02_length.sort((a,b) => {
        if(a.valor < b.valor){
          return 1;
        }
        if(a.valor > b.valor){
          return -1;
        }
        return 0;
    });
    let Q02_filtro = Q02_length.filter(a => (a.valor > 0));
    let dataQ02 = Q02_filtro.map(a => (a.valor));
    let labelsQ02 = Q02_filtro.map(a => (a.tipo));
    // //console.log(labelsQ02);
    // //console.log(dataQ02);

    let dataGraficoBlocoQ02 = {
      div:"graficoBlocoQ02",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      eixoX: labelsQ02,
      series: [{
        type: undefined,
        name: '%',
        data: dataQ02

      }],
    };

    var graficoBlocoQ02 = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoQ02);

    //--------------------------------------------------------------------------------
    //---------------QUESTÃO 04-------------------------
    let Q04_length  = [
      {tipo:'Conhece', valor: array[itemAtual].Q04.sim.filter(item => item == "1").length/array[itemAtual].Q04.sim.length*100,},
      {tipo:'Não conhece', valor: array[itemAtual].Q04.nao.filter(item => item == "1").length/array[itemAtual].Q04.nao.length*100,},
    ];  
  
    let dataQ04 = Q04_length.map(a => (a.valor));
    let labelsQ04 = Q04_length.map(a => (a.tipo));
    //console.log(dataQ21);
    //console.log(labelsQ21);
  
    let dataGraficoBlocoQ04:any;
    dataGraficoBlocoQ04 = {
      div:"graficoBlocoQ04",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ04[0],
            y: dataQ04[0],
            className: 'highcharts-color-0',
          },
          {
            type: undefined,
            name: labelsQ04[1],
            y: dataQ04[1],
            className: 'highcharts-color-2',
          }],
      }]
    };
    var graficoBlocoQ04 = this.graficos.plotarGraficoPizza(dataGraficoBlocoQ04);

    //--------------------------------------------------------------------------------
    //---------------QUESTÃO 05-------------------------
    let Q05_length  = [
      {tipo:'Sim', valor: array[itemAtual].Q05.sim.filter(item => item == "1").length/array[itemAtual].Q05.sim.length*100,},
      {tipo:'Não', valor: array[itemAtual].Q05.nao.filter(item => item == "1").length/array[itemAtual].Q05.nao.length*100,},
    ];  
  
    let dataQ05 = Q05_length.map(a => (a.valor));
    let labelsQ05 = Q05_length.map(a => (a.tipo));
    //console.log(dataQ21);
    //console.log(labelsQ21);
  
    let dataGraficoBlocoQ05:any;
    dataGraficoBlocoQ05 = {
      div:"graficoBlocoQ05",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      // eixoY: "% de respostas SIM",
      // eixoX: [labelsQ31],
      series: [{
        data:[{
            type: undefined,
            name: labelsQ05[0],
            y: dataQ05[0],
            className: 'highcharts-color-0',
          },
          {
            type: undefined,
            name: labelsQ05[1],
            y: dataQ05[1],
            className: 'highcharts-color-2',
          }],
      }]
    };
    var graficoBlocoQ05 = this.graficos.plotarGraficoPizza(dataGraficoBlocoQ05);

    //------------------------------------------
    //-----------------Questão 06---------------
    let Q06_length  = [
      {tipo:'Assistência técnica e extensão rural', valor: array[itemAtual].Q06.extensao_rural.filter(item => item == "1").length/array[itemAtual].Q06.extensao_rural.length*100,},
      {tipo:'Da prefeitura', valor: array[itemAtual].Q06.prefeitura.filter(item => item == "1").length/array[itemAtual].Q06.prefeitura.length*100,},
      {tipo:'Do estado pela EMATER', valor: array[itemAtual].Q06.estado.filter(item => item == "1").length/array[itemAtual].Q06.estado.length*100,},
      {tipo:'ONGs', valor: array[itemAtual].Q06.ongs.filter(item => item == "1").length/array[itemAtual].Q06.ongs.length*100},
      {tipo:'PAA', valor: array[itemAtual].Q06.paa.filter(item => item == "1").length/array[itemAtual].Q06.paa.length*100},
      {tipo:'PNAE', valor: array[itemAtual].Q06.pnae.filter(item => item == "1").length/array[itemAtual].Q06.pnae.length*100},
      {tipo:'Programa garantia Safra', valor: array[itemAtual].Q06.garantia_safra.filter(item => item == "1").length/array[itemAtual].Q06.garantia_safra.length*100},
      {tipo:'Promoção de feiras livres', valor: array[itemAtual].Q06.feiras_livres.filter(item => item == "1").length/array[itemAtual].Q06.feiras_livres.length*100},
      {tipo:'Cursos e palestras técnicas', valor: array[itemAtual].Q06.cursos.filter(item => item == "1").length/array[itemAtual].Q06.cursos.length*100},
      {tipo:'Estradas rurais', valor: array[itemAtual].Q06.estradas_rurais.filter(item => item == "1").length/array[itemAtual].Q06.estradas_rurais.length*100},
      
    ];  
  
    Q06_length = Q06_length.sort((a,b) => {
        if(a.valor < b.valor){
          return 1;
        }
        if(a.valor > b.valor){
          return -1;
        }
        return 0;
    });
    
    let Q06_filtro = Q06_length.filter(a => (a.valor > 0));
    let dataQ06 = Q06_filtro.map(a => (a.valor));
    let labelsQ06 = Q06_filtro.map(a => (a.tipo));
    // //console.log(labelsQ18);
    // //console.log(dataQ18);

    let dataGraficoBlocoQ06 = {
      div:"graficoBlocoQ06",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      eixoY: "% de respostas SIM",
      eixoX: labelsQ06,
      series: [{
        type: undefined,
        name: '%',
        data: dataQ06

      }],
    };
    var graficoBlocoQ06 = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoQ06);

    let array_plots = [graficoBlocoQ01, graficoBlocoQ02, graficoBlocoQ04, graficoBlocoQ05, graficoBlocoQ06]; 
    this.graficos.converterSVGtoPNG(array_plots, 0).then((data) =>{
        //  //console.log(data);
        this.listaGraficos = data;
        resolve();
      });
    })
  }

  public montarDocDefinition()
  {
    this.docDefinition = {
      content: [
        this.conteudo
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
          
        }
      }
    }
  }

  public getDoc()
  {
    return this.docDefinition;
  }

  downloadPdf() {
    let docDefinition = this.getDoc();
    // let grafico_width = this.csvProvider.getWidth();
    this.pdfObj = pdfMake.createPdf(docDefinition);
  
    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });
 
        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }
  

}



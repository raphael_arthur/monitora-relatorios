import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrefeiturasPage } from './prefeituras';

@NgModule({
  declarations: [
    PrefeiturasPage,
  ],
  imports: [
    IonicPageModule.forChild(PrefeiturasPage),
  ],
})
export class PrefeiturasPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform  } from 'ionic-angular';
import { Http } from '@angular/http';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { GraficosProvider } from '../../providers/graficos/graficos';
import { MontarCsvProvider } from '../../providers/montar-csv/montar-csv';

import { Printer, PrintOptions } from '@ionic-native/printer';

import * as papa from 'papaparse';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
 

  // csvData: any[] = [];
  // headerRow: any[] = [];

  letterObj = {
    to: '',
    from: '',
    text: ''
  }
 
  pdfObj = null;

  listaGraficos:any;
  constructor(
    public csvProvider:MontarCsvProvider, 
    private printer: Printer, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private http: Http, 
    private plt: Platform, 
    private file: File, 
    private fileOpener: FileOpener, 
    public graficos:GraficosProvider
    ) {
    this.csvProvider.readCsvData();
    
  }

  

  createPdf() {
    var conteudo = [];

    // for(let row of this.csvProvider.csvData)
    // {
      
    //   let blocoB = `
    //     Qual a duração (em minutos) de cada entrevista?
    //   `;

    //   let blocoC = `
    //     B) ANALÍTICO/CONSOLIDADO

    //     Qual a variação do número de animais nas propriedades entrevistadas? Aparecem valores extremos?
    //   `;

    //   let blocoD = `
    //     Qual a renda anual da produção agropecuária das famílias entrevistadas? Aparecem valores extremos?
    //   `;

    //   let blocoE = `
    //     Qual a renda anual da produção não agrícola das famílias entrevistadas? Aparecem valores extremos?
    //   `;

    //   let blocoF = `
    //     Qual a renda anual da produção não agrícola e do trabalho remunerado das famílias entrevistadas? Aparecem valores extremos?
    //   `;

    //   let blocoG = `
    //     Qual a renda anual dos auxílios recebidos pelas famílias entrevistadas? Aparecem valores extremos?
    //   `;

    //   let blocoH = `
    //     Qual a renda anual de outros rendimentos recebidos pelas famílias entrevistadas? Aparecem valores extremos?
    //   `;
    //   conteudo.push(
    //     { image:this.listaGraficos, width: 500 },
    //     { text: blocoB, style: 'story', alignment: 'left'},
    //     { image:this.listaGraficos, width: 500 },
    //     { text: blocoC, style: 'story', alignment: 'left'},
    //     { image:this.listaGraficos, width: 500 },
    //     { text: blocoD, style: 'story', alignment: 'left'},
    //     { image:this.listaGraficos, width: 500 },
    //     { text: blocoE, style: 'story', alignment: 'left'},
    //     { image:this.listaGraficos, width: 500 },
    //     { text: blocoF, style: 'story', alignment: 'left'},
    //     { image:this.listaGraficos, width: 500 },
    //     { text: blocoG, style: 'story', alignment: 'left'},
    //     { image:this.listaGraficos, width: 500 },
    //     { text: blocoH, style: 'story', alignment: 'left'},
    //     { image:this.listaGraficos, width: 500 },
    //   )
    // }
    conteudo.push(
      { image:this.listaGraficos, width: 500 },
    );
    var docDefinition = {
      content: [
        // { text: 'Relatório nível local', style: 'header', alignment: 'center'  },
 
        // {
        //   text: conteudo, style: 'story', alignment: 'left'
        // },

        // { 
        //   image:this.listaGraficos,
        //   width: 500,
        // }
        conteudo
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }
    }
    this.pdfObj = pdfMake.createPdf(docDefinition);
  }
 
  downloadPdf() {
    let docDefinition = this.csvProvider.getDoc();
    // let grafico_width = this.csvProvider.getWidth();
    this.pdfObj = pdfMake.createPdf(docDefinition);
  
    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });
 
        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }

  ionViewDidLoad()
  {
    var self = this;
      let dataGraficolinha = {
        div:"graficoBlocoA",
        titulo:"",
        subtitulo:"",
        // eixoX: "dinamico conforme tabela TODO",
        eixoY: "Número de entrevistas",
        series: [{
          type: undefined,
          name: 'Installation',
          data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
      }, {
          type: undefined,
          name: 'Manufacturing',
          data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
      }, {
          type: undefined,
          name: 'Sales & Distribution',
          data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
      }, {
          type: undefined,
          name: 'Project Development',
          data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
      }, {
          type: undefined,
          name: 'Other',
          data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
      }],
    }

    let dataGraficoBlocoB = {
      div:"graficoBlocoB",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      eixoY: "Número de entrevistas",
      series: [{
        name: 'Tokyo',
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

    }, {
        name: 'New York',
        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

    }, {
        name: 'London',
        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

    }, {
        name: 'Berlin',
        data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

    }],
  }
    var graficoBlocoC = this.graficos.plotarGraficoBoxplot("boxplot",dataGraficoBlocoB);
  }


}

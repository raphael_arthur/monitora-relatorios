interface DataNumero{
    data:string;
    numero:number;
}

export class OS {
    info_os:any;
    
    constructor(os:any, concluido_em_branco:any, cpf_entrevistador:string, nome_entrevistador: string )
    {
        this.info_os = {
            ordem_servico_id : os,
            concluido_em_branco : [concluido_em_branco],
            cpf_entrevistador : cpf_entrevistador,
            nome_entrevistador : nome_entrevistador,
            cpf : [],
            nome : [],
            data : [],
            quant_horas : [],
            animais : [],
            quantidade_animais: [],
            // animais_2 : [],
            // quantidade_animais_2 : [],
            // animais_3 : [],
            // quantidade_animais_3 : [],
            renda_animal :  [],
            // renda_animal_2 : [],
            // renda_animal_3 : [],
            producao_agropecuaria : {
                producao_animal: [],
                der_producao_animal: [],
                producao_vegetal: [],
                der_producao_vegetal: [],
                consumo_familiar:[]
            },
            nao_agricola : [],
            trabalho : {
                temporario_externo: [],
                permanente_externo: [],
            },
            // trabalho_2 : [],
            auxilios_recebidos : {
                bolsa_familia: [],
                emergencia_calamidade: [],
                seguro_defeso: [],
                salario_maternidade: [],
                saude_etc: [],
            },
            // auxilios_recebidos_2 : [],
            // auxilios_recebidos_3 : [],
            // auxilios_recebidos_4 : [],
            // auxilios_recebidos_5 : [],
            outros_rendimentos:{
                aposentadoria:[], 
                pensao:[],
                doacao:[],
                aluguel:[]
              },
            // outros_rendimentos_2 : [],
            // outros_rendimentos_3 : [],
            // outros_rendimentos_4 : [],
            renda_anual_total : [],
            //Dados analíticos:
            media_duracao_entrevistas: 0,
            desvio_padrao_entrevistas: 0,
            numero_entrevistas: [],
            duracao_minima_entrevista: 0,
            duracao_maxima_entrevista: 0
        }
    }

    public addInfoEntrevista(dadosEntrevista:any)
    {
        this.info_os.cpf.push(dadosEntrevista.cpf);
        this.info_os.nome.push(dadosEntrevista.nome);
        this.info_os.data.push(dadosEntrevista.data);
        let time = this.formatTime(this.timestrToSec(dadosEntrevista.quant_horas));
        if(!isNaN(time)){
            this.info_os.quant_horas.push(time);
        }
        else
        {
            console.log(time);
        }
        

        this.info_animais(dadosEntrevista);
        this.info_renda_animal(dadosEntrevista.renda_animal);
        this.info_producao_agropecuaria(dadosEntrevista.producao_agropecuaria);
        this.info_trabalho(dadosEntrevista.trabalho);
        this.info_auxilio(dadosEntrevista.auxilios_recebidos);
        this.info_outros_rendimentos(dadosEntrevista.outros_rendimentos);

        if(dadosEntrevista.nao_agricola != undefined )
        {
            this.info_os.nao_agricola.push(Number(dadosEntrevista.nao_agricola));
        }

        if(dadosEntrevista.renda_anual_total != undefined )
        {
            this.info_os.renda_anual_total.push(Number(dadosEntrevista.renda_anual_total));
        }
        
    }

    public addInfoEntrevistaRegional(dadosEntrevista:any)
    {
        for(let cpf of dadosEntrevista.cpf){
            this.info_os.cpf.push(cpf);
        }

        for(let nome of dadosEntrevista.nome){
            this.info_os.nome.push(nome);
        }
        
        for(let data of dadosEntrevista.data){
            this.info_os.data.push(data);
        }

        for(let quant_horas of dadosEntrevista.quant_horas){
            if(!isNaN(quant_horas))
            {
                this.info_os.quant_horas.push(quant_horas);
            }
            
        }

        this.info_animais_regional(dadosEntrevista);
        this.info_renda_animal(dadosEntrevista.renda_animal);
        this.info_producao_agropecuaria_regional(dadosEntrevista.producao_agropecuaria);
        this.info_trabalho_regional(dadosEntrevista.trabalho);
        this.info_auxilio_regional(dadosEntrevista.auxilios_recebidos);
        this.info_outros_rendimentos_regional(dadosEntrevista.outros_rendimentos);

        if(dadosEntrevista.nao_agricola != undefined )
        {
            for(let elemento of dadosEntrevista.nao_agricola)
            {
                this.info_os.nao_agricola.push(elemento);
            }
            
        }

        if(dadosEntrevista.renda_anual_total != undefined )
        {
            for(let elemento of dadosEntrevista.renda_anual_total)
            {
                this.info_os.renda_anual_total.push(elemento);
            }
            
        }
        
    }

    public info_animais(dadosEntrevista:any)
    {

        if(this.info_os.animais.length > 0)
        {
            let flagAnimal = false;
            // let indexFlag = 0;
            for(let i=0;i < dadosEntrevista.animais.length; i++)
            {
                flagAnimal = false;

                for(let j=0; j < this.info_os.animais.length; j++)
                {
                    if(dadosEntrevista.animais[i] != "")
                    {
                        if(dadosEntrevista.animais[i] == this.info_os.animais[j])
                        {
                            if(this.info_os.quantidade_animais[j] != undefined)
                            {
                                this.info_os.quantidade_animais[j].push(Number(dadosEntrevista.quantidade_animais[i]));
                            }
                            else
                            {
                                this.info_os.quantidade_animais.push([Number(dadosEntrevista.quantidade_animais[i])]);
                            }
                            
                            flagAnimal = true;
                            break;
                        }
                        else
                        {
                            flagAnimal = false;
                        }
                    }
                }
                if(!flagAnimal && dadosEntrevista.animais[i] != "")
                {
                    this.info_os.animais.push(dadosEntrevista.animais[i]);
                    this.info_os.quantidade_animais.push([Number(dadosEntrevista.quantidade_animais[i])]);
                }
            }
        }
        else
        {
            for(let animais of dadosEntrevista.animais)
            {
                if(animais != "")
                {
                    this.info_os.animais.push(animais);
                }
                
            }

            for(let quantidade_animal of dadosEntrevista.quantidade_animais)
            {
                if(quantidade_animal != "") this.info_os.quantidade_animais.push([Number(quantidade_animal)]);
            }
            
        }
    }

    public info_animais_regional(dadosEntrevista:any)
    {

        if(this.info_os.animais.length > 0)
        {
            let flagAnimal = false;
            // let indexFlag = 0;
            for(let i=0;i < dadosEntrevista.animais.length; i++)
            {
                flagAnimal = false;

                for(let j=0; j < this.info_os.animais.length; j++)
                {
                    if(dadosEntrevista.animais[i] != "")
                    {
                        if(dadosEntrevista.animais[i] == this.info_os.animais[j])
                        {
                            if(this.info_os.quantidade_animais[j] != undefined)
                            {
                                let concat = this.info_os.quantidade_animais[j].concat((dadosEntrevista.quantidade_animais[i]));
                                this.info_os.quantidade_animais[j] = concat;
                            }
                            else
                            {
                                this.info_os.quantidade_animais.push((dadosEntrevista.quantidade_animais[i]));
                            }
                            
                            flagAnimal = true;
                            break;
                        }
                        else
                        {
                            flagAnimal = false;
                        }
                    }
                }
                if(!flagAnimal && dadosEntrevista.animais[i] != "")
                {
                    this.info_os.animais.push(dadosEntrevista.animais[i]);
                    this.info_os.quantidade_animais.push(dadosEntrevista.quantidade_animais[i]);
                }
            }
        }
        else
        {
            for(let animais of dadosEntrevista.animais)
            {
                if(animais != "")
                {
                    this.info_os.animais.push(animais);
                }
                
            }

            for(let quantidade_animal of dadosEntrevista.quantidade_animais)
            {
                if(quantidade_animal != "") this.info_os.quantidade_animais.push((quantidade_animal));
            }
            
        }
    }

    public info_renda_animal(renda_animal:any)
    {
        if(renda_animal != undefined)
        {
            for(let valor of renda_animal)
            {
                if(valor != "" && valor != 0)
                {
                    this.info_os.renda_animal.push(Number(valor)) ;
                }
            }
        }
    }

    public info_producao_agropecuaria(producao:any)
    {
        // producao_animal: this.csvData[i][this.producao_animal],
        //   der_producao_animal: this.csvData[i][this.der_producao_animal],
        //   producao_vegetal: this.csvData[i][this.producao_vegetal],
        //   der_producao_vegetal: this.csvData[i][this.der_producao_vegetal],
        //   consumo_familiar:this.csvData[i][this.consumo_familiar]
        if(producao.producao_animal != undefined)
        {
            this.info_os.producao_agropecuaria.producao_animal.push(Number(producao.producao_animal));
        }
        if(producao.der_producao_animal != undefined)
        {
            this.info_os.producao_agropecuaria.der_producao_animal.push(Number(producao.der_producao_animal));
        }
        if(producao.producao_vegetal != undefined)
        {
            this.info_os.producao_agropecuaria.producao_vegetal.push(Number(producao.producao_vegetal));
        }
        if(producao.der_producao_vegetal != undefined)
        {
            this.info_os.producao_agropecuaria.der_producao_vegetal.push(Number(producao.der_producao_vegetal));
        }
        if(producao.consumo_familiar != undefined)
        {
            this.info_os.producao_agropecuaria.consumo_familiar.push(Number(producao.consumo_familiar));
        }
        // if(producao != undefined)
        // {
        //     for(let valor of producao)
        //     {
        //         if(valor != "" && valor != 0)
        //         {
        //             this.info_os.producao_agropecuaria.push(Number(valor));
        //         }
        //     }
        // }
    }

    public info_producao_agropecuaria_regional(producao:any)
    {
        // producao_animal: this.csvData[i][this.producao_animal],
        //   der_producao_animal: this.csvData[i][this.der_producao_animal],
        //   producao_vegetal: this.csvData[i][this.producao_vegetal],
        //   der_producao_vegetal: this.csvData[i][this.der_producao_vegetal],
        //   consumo_familiar:this.csvData[i][this.consumo_familiar]
        if(producao.producao_animal != undefined)
        {
            for(let array of producao.producao_animal)
            {
                this.info_os.producao_agropecuaria.producao_animal.push(Number(array));
            }
            
        }
        if(producao.der_producao_animal != undefined)
        {
            for(let array of producao.der_producao_animal)
            {
                this.info_os.producao_agropecuaria.der_producao_animal.push(array);
            }   
        }
        if(producao.producao_vegetal != undefined)
        {
            for(let array of producao.producao_vegetal)
            {
                this.info_os.producao_agropecuaria.producao_vegetal.push(array);
            }
        }
        if(producao.der_producao_vegetal != undefined)
        {
            for(let array of producao.der_producao_vegetal)
            {
                this.info_os.producao_agropecuaria.der_producao_vegetal.push(array);
            }
            
        }
        if(producao.consumo_familiar != undefined)
        {
            for(let array of producao.consumo_familiar)
            {
                this.info_os.producao_agropecuaria.consumo_familiar.push(array);
            }
            
        }
    }

    public info_trabalho(trabalho:any)
    {
        if(trabalho.temporario_externo != undefined)
        {
            this.info_os.trabalho.temporario_externo.push(Number(trabalho.temporario_externo));
        }
        if(trabalho.permanente_externo != undefined)
        {
            this.info_os.trabalho.permanente_externo.push(Number(trabalho.permanente_externo));
        }
        
    }

    public info_trabalho_regional(trabalho:any)
    {
        if(trabalho.temporario_externo != undefined)
        {
            for(let array of trabalho.temporario_externo)
            {
                this.info_os.trabalho.temporario_externo.push(array);
            }
            
        }
        if(trabalho.permanente_externo != undefined)
        {
            for(let array of trabalho.permanente_externo)
            {
                this.info_os.trabalho.permanente_externo.push(array);
            }
        }
    }

    public info_auxilio(auxilio:any)
    {
        if(auxilio.bolsa_familia != undefined)
        {
            this.info_os.auxilios_recebidos.bolsa_familia.push(Number(auxilio.bolsa_familia));
        }
        if(auxilio.emergencia_calamidade != undefined)
        {
            this.info_os.auxilios_recebidos.emergencia_calamidade.push(Number(auxilio.emergencia_calamidade));
        }
        if(auxilio.seguro_defeso != undefined)
        {
            this.info_os.auxilios_recebidos.seguro_defeso.push(Number(auxilio.seguro_defeso));
        }
        if(auxilio.salario_maternidade != undefined)
        {
            this.info_os.auxilios_recebidos.salario_maternidade.push(Number(auxilio.salario_maternidade));
        }
        if(auxilio.saude_etc != undefined)
        {
            this.info_os.auxilios_recebidos.saude_etc.push(Number(auxilio.saude_etc));
        }

    }

    public info_auxilio_regional(auxilio:any)
    {
        if(auxilio.bolsa_familia != undefined)
        {
            for(let array of auxilio.bolsa_familia)
            {
                this.info_os.auxilios_recebidos.bolsa_familia.push(array);
            }
        }
        if(auxilio.emergencia_calamidade != undefined)
        {
            for(let array of auxilio.emergencia_calamidade)
            {
                this.info_os.auxilios_recebidos.emergencia_calamidade.push(array);
            }
        }
        if(auxilio.seguro_defeso != undefined)
        {
            for(let array of auxilio.seguro_defeso)
            {
                this.info_os.auxilios_recebidos.seguro_defeso.push(array);
            }
        }
        if(auxilio.salario_maternidade != undefined)
        {
            for(let array of auxilio.salario_maternidade)
            {
                this.info_os.auxilios_recebidos.salario_maternidade.push(array);
            }
        }
        if(auxilio.saude_etc != undefined)
        {
            for(let array of auxilio.saude_etc)
            {
                this.info_os.auxilios_recebidos.saude_etc.push(array);
            }
        }
    }

    public info_outros_rendimentos(rendimentos:any)
    {

        if(rendimentos.aposentadoria != undefined)
        {
            this.info_os.outros_rendimentos.aposentadoria.push(Number(rendimentos.aposentadoria));
        }
        if(rendimentos.pensao != undefined)
        {
            this.info_os.outros_rendimentos.pensao.push(Number(rendimentos.pensao));
        }
        if(rendimentos.doacao != undefined)
        {
            this.info_os.outros_rendimentos.doacao.push(Number(rendimentos.doacao));
        }
        if(rendimentos.aluguel != undefined)
        {
            this.info_os.outros_rendimentos.aluguel.push(Number(rendimentos.aluguel));
        }
    }

    public info_outros_rendimentos_regional(rendimentos:any)
    {

        if(rendimentos.aposentadoria != undefined)
        {
            for(let array of rendimentos.aposentadoria)
            {
                this.info_os.outros_rendimentos.aposentadoria.push(array);
            }
            
        }
        if(rendimentos.pensao != undefined)
        {
            for(let array of rendimentos.pensao)
            {
                this.info_os.outros_rendimentos.pensao.push(array);
            }
            
        }
        if(rendimentos.doacao != undefined)
        {
            for(let array of rendimentos.doacao)
            {
                this.info_os.outros_rendimentos.doacao.push(array);
            }   
            
        }
        if(rendimentos.aluguel != undefined)
        {
            for(let array of rendimentos.aluguel)
            {
                this.info_os.outros_rendimentos.aluguel.push(array);
            }
        }
    }

    public calcular_entrevistas()
    {
        let media_duracao = 0;
        // for(let entrevista of this.info_os.quant_horas)
        // {
        //     media_duracao += this.timestrToSec(entrevista);
        // }
        // this.info_os.media_duracao_entrevistas = this.formatTime(media_duracao/(this.info_os.quant_horas.length));
        // console.log(this.info_os.media_duracao_entrevistas);

        let media = this.info_os.quant_horas.reduce((total, valor) => total+(valor)/this.info_os.quant_horas.length, 0);
        let variancia = this.info_os.quant_horas.reduce((total, valor) => total + Math.pow(media - (valor), 2)/this.info_os.quant_horas.length, 0);
        let desvioPadrao = Math.sqrt(variancia);

        this.info_os.media_duracao_entrevistas = (media).toFixed(4);
        this.info_os.desvio_padrao_entrevistas = (desvioPadrao).toFixed(4);

        let array_dataNumero = [];
        let temp:any;
        if(this.info_os.data.length > 0)
        {
            for(let data of this.info_os.data)
            {
                if(array_dataNumero.length > 0)
                {
                    let flagData = false;
                    for(let i = 0; i < array_dataNumero.length; i++)
                    {
                        if(array_dataNumero[i][0] == data)
                        {
                            array_dataNumero[i][1]++;
                            flagData = true;
                            break;
                        }
                    }
                    if(!flagData)
                    {
                        temp = [data, 1]
                        array_dataNumero.push(temp);
                    }
                }
                else
                {
                    temp = [data, 1]
                    array_dataNumero.push(temp);
                }
            }
        }
        this.info_os.numero_entrevistas = array_dataNumero;
    }

    public calcular_entrevistas_regional()
    {
        var self = this;
        let media_duracao = 0;
        let quant_horas_ordenados = [...self.info_os.quant_horas];
        quant_horas_ordenados.sort((a,b) => a-b);
        let media = self.info_os.quant_horas.reduce((total, valor) => 
            total+(valor)/self.info_os.quant_horas.length, 0);
        let variancia = self.info_os.quant_horas.reduce((total, valor) => total + Math.pow(media - (valor), 2)/self.info_os.quant_horas.length, 0);
        let desvioPadrao = Math.sqrt(variancia);

        self.info_os.media_duracao_entrevistas = (media).toFixed(4);
        self.info_os.desvio_padrao_entrevistas = (desvioPadrao).toFixed(4);

        let array_dataNumero = [];
        let temp:any;
        if(self.info_os.data.length > 0)
        {
            for(let data of self.info_os.data)
            {
                if(array_dataNumero.length > 0)
                {
                    let flagData = false;
                    for(let i = 0; i < array_dataNumero.length; i++)
                    {
                        if(array_dataNumero[i][0] == data)
                        {
                            array_dataNumero[i][1]++;
                            flagData = true;
                            break;
                        }
                    }
                    if(!flagData)
                    {
                        temp = [data, 1]
                        array_dataNumero.push(temp);
                    }
                }
                else
                {
                    temp = [data, 1]
                    array_dataNumero.push(temp);
                }
            }
        }
        self.info_os.numero_entrevistas = array_dataNumero;
        self.info_os.duracao_minima_entrevista = quant_horas_ordenados[0];
        self.info_os.duracao_maxima_entrevista = quant_horas_ordenados[quant_horas_ordenados.length-1];
    }

    public timestrToSec(timestr) {
        var parts = timestr.split(":");
        return (parts[0] * 3600) +
               (parts[1] * 60) +
               (+parts[2]);
      }
      
      public pad(num) {
        if(num < 10) {
          return "0" + num;
        } else {
          return "" + num;
        }
      }
      
      //para minutos:
      public formatTime(seconds) {
        // return [this.pad(Math.floor(seconds/60)),
        //     // this.pad(Math.floor(seconds/60)%60),
        //     this.pad(seconds%60),
        //         ].join(":");
        return Number(this.pad(Math.floor(seconds/60)));
      }
      
}
export class Prefeitura{

    public entrevistador:string;
    public uf:string;
    public os:string;

    public Q01:any;
    public Q02:any;
    public Q04:any;
    public Q05:any;
    public Q06:any;

    constructor(cabecalho:any){
        this.entrevistador = cabecalho.entrevistador;
        this.uf = cabecalho.uf;
        this.os = cabecalho.os;
        
        this.Q01 = {
            saude: [],
            educacao: [],
            pobreza: [],
            emprego: [],
            seguranca_alimentar:[],
            habitacao: [],
            transportes: [],
            seguranca: [],
            previdencia: [],
            ater: []
        };

        this.Q02 = {
            bolsa_familia: [],
            luz_no_campo: [],
            luz_para_todos: [],
            cisterna_1: [],
            cisterna_2: [],
            extensao_rural: [],
            financiamento_agricola: [],
            pronaf: [],
            paa: [],
            pnae: [],
        };

        this.Q04 = {
            sim: [],
            nao: [],
        };

        this.Q05 = {
            sim: [],
            nao: [],
        };

        this.Q06 = {
            extensao_rural: [],
            prefeitura: [],
            estado: [],
            ongs: [],
            paa: [],
            pnae: [],
            garantia_safra: [],
            feiras_livres: [],
            cursos: [],
            estradas_rurais: [],
        };

    }

    public addInfoEntrevista(Q01:any, Q02:any, Q04:any, Q08:any, Q19:any )
    {
        this.Q01.saude.push(Q01.saude);
        this.Q01.educacao.push(Q01.educacao);
        this.Q01.pobreza.push(Q01.pobreza);
        this.Q01.emprego.push(Q01.emprego);
        this.Q01.seguranca_alimentar.push(Q01.seguranca_alimentar);
        this.Q01.habitacao.push(Q01.habitacao);
        this.Q01.transportes.push(Q01.transportes);
        this.Q01.seguranca.push(Q01.seguranca);
        this.Q01.previdencia.push(Q01.previdencia);
        this.Q01.ater.push(Q01.ater);

        this.Q02.bolsa_familia.push(Q02.bolsa_familia);
        this.Q02.luz_no_campo.push(Q02.luz_no_campo);
        this.Q02.luz_para_todos.push(Q02.luz_para_todos);
        this.Q02.cisterna_1.push(Q02.cisterna_1);
        this.Q02.cisterna_2.push(Q02.cisterna_2);
        this.Q02.extensao_rural.push(Q02.extensao_rural);
        this.Q02.financiamento_agricola.push(Q02.financiamento_agricola);
        this.Q02.pronaf.push(Q02.pronaf);
        this.Q02.paa.push(Q02.paa);
        this.Q02.pnae.push(Q02.pnae);

        this.Q04.sim.push(Q04.sim);
        this.Q04.nao.push(Q04.nao);

        this.Q05.sim.push(Q08.sim);
        this.Q05.nao.push(Q08.nao);

        this.Q06.extensao_rural.push(Q19.extensao_rural);
        this.Q06.prefeitura.push(Q19.prefeitura);
        this.Q06.estado.push(Q19.estado);
        this.Q06.ongs.push(Q19.ongs);
        this.Q06.paa.push(Q19.paa);
        this.Q06.pnae.push(Q19.pnae);
        this.Q06.garantia_safra.push(Q19.garantia_safra);
        this.Q06.feiras_livres.push(Q19.feiras_livres);
        this.Q06.cursos.push(Q19.cursos);
        this.Q06.estradas_rurais.push(Q19.estradas_rurais);
    }

    public addRegional(Q01:any, Q02:any, Q04:any, Q05:any, Q06:any)
    {
        for(let resp of Q01.saude) {this.Q01.saude.push(resp)};
        for(let resp of Q01.educacao){this.Q01.educacao.push(resp);}
        for(let resp of Q01.pobreza){this.Q01.pobreza.push(resp);}
        for(let resp of Q01.emprego){this.Q01.emprego.push(resp);}
        for(let resp of Q01.seguranca_alimentar){this.Q01.seguranca_alimentar.push(resp);}
        for(let resp of Q01.habitacao){this.Q01.habitacao.push(resp);}
        for(let resp of Q01.transportes){this.Q01.transportes.push(resp);}
        for(let resp of Q01.seguranca){this.Q01.seguranca.push(resp);}
        for(let resp of Q01.previdencia){this.Q01.previdencia.push(resp);}
        for(let resp of Q01.ater){this.Q01.ater.push(resp);}

        for(let resp of Q02.bolsa_familia){this.Q02.bolsa_familia.push(resp);}
        for(let resp of Q02.luz_no_campo){this.Q02.luz_no_campo.push(resp);}
        for(let resp of Q02.luz_para_todos){this.Q02.luz_para_todos.push(resp);}
        for(let resp of Q02.cisterna_1){this.Q02.cisterna_1.push(resp);}
        for(let resp of Q02.cisterna_2){this.Q02.cisterna_2.push(resp);}
        for(let resp of Q02.extensao_rural){this.Q02.extensao_rural.push(resp);}
        for(let resp of Q02.financiamento_agricola){this.Q02.financiamento_agricola.push(resp);}
        for(let resp of Q02.pronaf){this.Q02.pronaf.push(resp);}
        for(let resp of Q02.paa){this.Q02.paa.push(resp);}
        for(let resp of Q02.pnae){this.Q02.pnae.push(resp);}

        for(let resp of Q04.sim){this.Q04.sim.push(resp);}
        for(let resp of Q04.nao){this.Q04.nao.push(resp);}

        for(let resp of Q05.sim){this.Q05.sim.push(resp);}
        for(let resp of Q05.nao){this.Q05.nao.push(resp);}

        for(let resp of Q06.extensao_rural){this.Q06.extensao_rural.push(resp);}
        for(let resp of Q06.prefeitura){this.Q06.prefeitura.push(resp);}
        for(let resp of Q06.estado){this.Q06.estado.push(resp);}
        for(let resp of Q06.ongs){this.Q06.ongs.push(resp);}
        for(let resp of Q06.paa){this.Q06.paa.push(resp);}
        for(let resp of Q06.pnae){this.Q06.pnae.push(resp);}
        for(let resp of Q06.garantia_safra){this.Q06.garantia_safra.push(resp);}
        for(let resp of Q06.feiras_livres){this.Q06.feiras_livres.push(resp);}
        for(let resp of Q06.cursos){this.Q06.cursos.push(resp);}
        for(let resp of Q06.estradas_rurais){this.Q06.estradas_rurais.push(resp);}
    }

}
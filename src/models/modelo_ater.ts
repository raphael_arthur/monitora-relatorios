
export class Ater{
    public info:any;
    public os:number;
    public cpf:number;
    public entrevistador:string;
    public data_entrevista:string;
    public uf:string;
    public Q14:any;
    public Q18:any;
    public Q19:any; 
    public Q21:any; 
    public Q22:any; 
    public Q23:any; 
    public Q31:any; 
    public Q32:any; 
    public Q34:any; 
    public Q35:any; 
    public Q36:any; 
    public Q38:any;
    

    constructor(aterCabecalho:any)
    {
        this.os = aterCabecalho.os;
        this.cpf = aterCabecalho.cpf;
        this.entrevistador = aterCabecalho.entrevistador;
        this.data_entrevista = aterCabecalho.data;
        this.uf = aterCabecalho.uf;

        this.Q14  = {
            bovinocultura:[],
            apicultura:[],
            caprinocultura:[],
            ovinocultura:[],
            suinocultura:[],
            avicultura: [],
            fruticultura: [],
            hortalicas: [],
            mandioca: [],
            milho: [],
            feijao: [],
            extrativismo: [],
            ensilagem: [],
            pesca: [],
            artesanato: [],
            beneficiamento: [],
            outros: [],
        }; 

        this.Q18 = {
            nova_forma_plantar: [],
            maquinas_agricolas: [],
            novo_animal: [],
            nova_forma_alimentacao_animal: [],
            alteracao_manejo: [],
            uso_irrigacao: [],
            cobertura_solo: [],
            agroecologia: [],
            outros: [],
        };
        this.Q19 = {
            armazenamento: [],
            transporte: [],
            processamento_alimentos: [],
            embalagem: [],
            canal_comercializacao: [],
            outros: [],
          };

        this.Q21 = []; this.Q22 = []; this.Q23 = []; this.Q31 = []; 
        this.Q32 = []; this.Q34 = []; this.Q35 = []; this.Q36 = []; this.Q38 = [];
    }

    public addInfoEntrevista(Q14:any, Q18:any, Q19:any, Q21:any, Q22:any, Q23:any, Q31:any, Q32:any, Q34:any, Q35:any, Q36:any, Q38:any)
    {
        this.Q14.bovinocultura.push(Q14.bovinocultura);
        this.Q14.apicultura.push(Q14.apicultura);
        this.Q14.caprinocultura.push(Q14.caprinocultura);
        this.Q14.ovinocultura.push(Q14.ovinocultura);
        this.Q14.suinocultura.push(Q14.suinocultura);
        this.Q14.avicultura.push(Q14.avicultura);
        this.Q14.fruticultura.push(Q14.fruticultura);
        this.Q14.hortalicas.push(Q14.hortalicas);
        this.Q14.mandioca.push(Q14.mandioca);
        this.Q14.milho.push(Q14.milho);
        this.Q14.feijao.push(Q14.feijao);
        this.Q14.extrativismo.push(Q14.extrativismo);
        this.Q14.ensilagem.push(Q14.ensilagem);
        this.Q14.pesca.push(Q14.pesca);
        this.Q14.artesanato.push(Q14.artesanato);
        this.Q14.beneficiamento.push(Q14.beneficiamento);
        this.Q14.outros.push(Q14.outros);

        this.Q18.nova_forma_plantar.push(Q18.nova_forma_plantar);
        this.Q18.maquinas_agricolas.push(Q18.maquinas_agricolas);
        this.Q18.novo_animal.push(Q18.novo_animal);
        this.Q18.nova_forma_alimentacao_animal.push(Q18.nova_forma_alimentacao_animal);
        this.Q18.alteracao_manejo.push(Q18.alteracao_manejo);
        this.Q18.uso_irrigacao.push(Q18.uso_irrigacao);
        this.Q18.cobertura_solo.push(Q18.cobertura_solo);
        this.Q18.agroecologia.push(Q18.agroecologia);
        this.Q18.outros.push(Q18.outros);

        this.Q19.armazenamento.push(Q19.armazenamento);
        this.Q19.transporte.push(Q19.transporte);
        this.Q19.processamento_alimentos.push(Q19.processamento_alimentos);
        this.Q19.embalagem.push(Q19.embalagem);
        this.Q19.canal_comercializacao.push(Q19.canal_comercializacao);
        this.Q19.outros.push(Q19.outros);

        this.Q21.push(Q21.resposta);
        this.Q22.push(Q22.resposta);
        this.Q23.push(Q23.resposta);
        this.Q31.push(Q31.resposta);
        this.Q32.push(Q32.resposta);
        this.Q34.push(Q34.resposta);
        this.Q35.push(Q35.resposta);
        this.Q36.push(Q36.resposta);
        this.Q38.push(Q38.resposta);

        
    }

    public addRegional(Q14:any, Q18:any, Q19:any, Q21:any, Q22:any, Q23:any, Q31:any, Q32:any, Q34:any, Q35:any, Q36:any, Q38:any )
    {
        //console.log(Q14);
        for(let resp of Q14.bovinocultura){ this.Q14.bovinocultura.push(resp);}
        for(let resp of Q14.apicultura){ this.Q14.apicultura.push(resp);}
        for(let resp of Q14.caprinocultura){ this.Q14.caprinocultura.push(resp);}
        for(let resp of Q14.ovinocultura){ this.Q14.ovinocultura.push(resp);}
        for(let resp of Q14.suinocultura){ this.Q14.suinocultura.push(resp);}
        for(let resp of Q14.avicultura){ this.Q14.avicultura.push(resp);}
        for(let resp of Q14.fruticultura){ this.Q14.fruticultura.push(resp);}
        for(let resp of Q14.hortalicas){ this.Q14.hortalicas.push(resp);}
        for(let resp of Q14.mandioca){ this.Q14.mandioca.push(resp);}
        for(let resp of Q14.milho){ this.Q14.milho.push(resp);}
        for(let resp of Q14.feijao){ this.Q14.feijao.push(resp);}
        for(let resp of Q14.extrativismo){ this.Q14.extrativismo.push(resp);}
        for(let resp of Q14.ensilagem){ this.Q14.ensilagem.push(resp);}
        for(let resp of Q14.pesca){ this.Q14.pesca.push(resp);}
        for(let resp of Q14.artesanato){ this.Q14.artesanato.push(resp);}
        for(let resp of Q14.beneficiamento){ this.Q14.beneficiamento.push(resp);}
        for(let resp of Q14.outros){ this.Q14.outros.push(resp);}

        //Q18
        for(let resp of Q18.nova_forma_plantar){ this.Q18.nova_forma_plantar.push(resp)};
        for(let resp of Q18.maquinas_agricolas){ this.Q18.maquinas_agricolas.push(resp)};
        for(let resp of Q18.novo_animal){ this.Q18.novo_animal.push(resp)};
        for(let resp of Q18.nova_forma_alimentacao_animal){ this.Q18.nova_forma_alimentacao_animal.push(resp)};
        for(let resp of Q18.alteracao_manejo){ this.Q18.alteracao_manejo.push(resp)};
        for(let resp of Q18.uso_irrigacao){ this.Q18.uso_irrigacao.push(resp)};
        for(let resp of Q18.cobertura_solo){ this.Q18.cobertura_solo.push(resp)};
        for(let resp of Q18.agroecologia){ this.Q18.agroecologia.push(resp)};
        for(let resp of Q18.outros){ this.Q18.outros.push(resp)};

        //Q19
        for(let resp of Q19.armazenamento){ this.Q19.armazenamento.push(resp) };
        for(let resp of Q19.transporte){ this.Q19.transporte.push(resp) };
        for(let resp of Q19.processamento_alimentos){ this.Q19.processamento_alimentos.push(resp) };
        for(let resp of Q19.embalagem){ this.Q19.embalagem.push(resp) };
        for(let resp of Q19.canal_comercializacao){ this.Q19.canal_comercializacao.push(resp) };
        for(let resp of Q19.outros){ this.Q19.outros.push(resp) };
        
        //Q21
        for(let resp of Q21.resposta) { this.Q21.push(resp) };

        //Q22
        for(let resp of Q22.resposta) { this.Q22.push(resp) };

        //Q22
        for(let resp of Q23.resposta) { this.Q23.push(resp) };

        //Q22
        for(let resp of Q31.resposta) { this.Q31.push(resp) };

        //Q22
        for(let resp of Q32.resposta) { this.Q32.push(resp) };

        //Q22
        for(let resp of Q34.resposta) { this.Q34.push(resp) };

        //Q22
        for(let resp of Q35.resposta) { this.Q35.push(resp) };

        //Q22
        for(let resp of Q36.resposta) { this.Q36.push(resp) };

        //Q22
        for(let resp of Q38.resposta) { this.Q38.push(resp) };
    
    
    
    
    }
}
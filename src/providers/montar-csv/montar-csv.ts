import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import * as papa from 'papaparse';
import {OS} from '../../models/modelo_os';
import { GraficosProvider } from '../graficos/graficos';
/*
  Generated class for the MontarCsvProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MontarCsvProvider {
   //DICIONARIO DO CABEÇALHO
   readonly ordem_servico_id = 0;
   readonly concluido_em_branco = 1;
   readonly cpf_entrevistador = 2;
   readonly nome_entrevistador = 3;
   readonly cpf = 4;
   readonly nome = 5;
   readonly data = 6;
   readonly quant_horas = 7;
   readonly animais_1 = 8;
   readonly quantidade_animais_1 = 9;
   readonly animais_2 = 10;
   readonly quantidade_animais_2 = 11;
   readonly animais_3 = 12;
   readonly quantidade_animais_3 = 13;
   readonly renda_animal_1 = 14;
   readonly renda_animal_2 = 15;
   readonly renda_animal_3 = 16;
   //Producao Agropecuaria//
   readonly producao_animal = 17;
   readonly der_producao_animal = 18;
   readonly producao_vegetal = 19;
   readonly der_producao_vegetal = 20;
   readonly consumo_familiar = 21;
   //-----------------------------//
   readonly nao_agricola = 22;
   readonly trabalho_temp_ext = 23;
   readonly trabalho_per_ext = 24;
   readonly bolsa_familia = 25;
   readonly emergencia_calamidade = 26;
   readonly seguro_defeso = 27;
   readonly saude_etc = 28;
   readonly salario_maternidade = 29;
   readonly aposentadoria = 30;
   readonly pensao = 31;
   readonly doacao = 32;
   readonly aluguel = 33;
   readonly renda_anual_total = 34;
   //---------------------------------------------------
  csvData: any[] = [];
  headerRow: any[] = [];

  conteudo:any = [];

  grafico_width = 0;
  public array_os:Array<OS>;
  public array_regional:Array<OS>;
  public array_nacional:Array<OS>;
  public supervisor_nacional:any;

  docDefinition: { content: any[][]; styles: { header: { fontSize: number; bold: boolean; }; subheader: { fontSize: number; bold: boolean; margin: number[]; }; story: { italic: boolean; alignment: string; width: string; }; }; };
  listaGraficos: any = [];

  supervisor_estadual:any;
  supervisor_nacional2: string[];
  constructor(public http: Http, public graficos:GraficosProvider) {
    // console.log('Hello MontarCsvProvider Provider');
    this.array_os = [];
    this.array_regional = [];
    this.supervisor_estadual = {
      "José Cleto de Araújo Alves":{
        index:0,
        nomes:["Cinthia Guimarães de Oliveira", 
        "Guilherme Belmonte",
        "John Carlos da Silva Pinto",
        "Luciano Flamorion da Cunha Moreira",
        "Wellington Bezerra Soares",
        "Ivanilson Ferreira da Silva"],
      },
      "Bruno Xavier Gomes": {
        index:1,
        nomes:[
        "Antônio Carlos Cerqueira Borges",
        "Antonio Milton de Oliveira Silva",
        "Geraldo Sérgio Silva Ferreira",
        "Hamilton Almeida Torres",
        "Hugo Fernando Barbosa da Silva",
        "José Raimundo Oliveira dos Santos",
        "Joseleine Mendes Barbosa Souza",
        "Monike Keila Sousa de Oliveira",
        "Paulo Henrique Carneiro de Oliveira",
        "Wellington dos Santos Dourado",
        "Antonio Carlos Cerqueira Borges"
      ]},
      "Paulo Matos Barreto":{
        index:2,
        nomes:[
        "Antônio Alberto Benevides S. Júnior",
        "Antônio Minelvino de Souza Neto" ,
        "Arnóbio Rodrigues da Silva" ,
        "Francisco das Chagas Martins Bezerra" ,
        "Johnathan Emanuel Pinto Bessa" ,
        "José Antônio Martins da Silva" ,
        "José Junior Gonçalves Noronha",
        "Manoel Vieira de Carvalho Filho" ,
        "Ramille Gonçalves de Souza" ,
        "Rodrigo de Andrade Rodrigues" ,
        "Yvon Saymon Canuto Vieira" ,
        "Antonio Alberto Benevides S. Júnior",
        "Ramille Gonçalves de Sousa",
        "Marciel da Silva Freire"
      ]},
      "Diego Capucho Cezana":{
        index:3,
        nomes:[
        "Daniele Batista Caetano",
        "Evaldo Nalli Ramos",
        "Sandra Madalena Valentim de Souza",
        'SANDRO SOARES TAVARES',
        'ADAILTON DUARTE LIMA',
        'ALEXSANDRO PEREIRA PEDROSA',
        'CELY MOURA AMARAL',
        'PAULO ROGÉRIO DE MEDEIROS SILVA',
        'SIDE CLAUDIO COSTA BARROS',
        
      ]},
      "Sandro Soares Tavares":{
        index:4,
        nomes:[
        'Adailton Duarte Lima',
        'Alexsandro Pereira Pedrosa',
        'Cely Moura Amaral',
        'Paulo Rogério de Medeiros Silva',
        'Side Claudio Costa Barros',
        "Silde Soares Tavares"
      ]},
      "Dinovan Queiroz de Almeida":{
        index:5,
        nomes:[
        'Adriana Viana Chagas',
        'Carlos Ruas Reis',
        'Fábio Ernani de Oliveira',
        'Fábio Luiz Becker',
        'Geraldo Aparecido Miguel Soares',
        'Hercules Vandy Durães da Fonseca',
        'João Eder Oliveira Almeida',
        'José Nilton Reis Santana',
        'Roselmiro Guimarães Entreportes',
        'Saulo Souza Matos',
        "Ezequiel Alves Martins"
      ]},
      "Julio Breno Vieira Lemos":{
        index:6,
        nomes:[
        'Adailson Bernardo dos Santos',
        'Bilac Soares de Oliveira',
        'Francisco de Luna Alves',
        'Leonel da Costa Azevedo',
        'Paulo Sergio Lins Dantas',
        'Robério Macêdo de Oliveira',
        'Victor Hugo Benevides Trigueiro Targino',
        'Lindinalva Oliveira de Paulo',
        'Paulo Lacerda de Oliveira',
        'Pablo Antonio de Sousa Monteiro',
        'Ana Palhano Freire Neta',
        'Jorismar dos Reis Fernandes'
      ]},
      "Edmar de Oliveira e Silva":{
        index:7,
        nomes:[
        'Antônio Florêncio Barros Medrado',
        'Eguimagno Rodrigues de Morais' ,
        'Esmar Esmeraldo Santos' ,
        'Francilene Pereira do Nascimento',
        'José Acevedo Alves Junior',
        'Júlio Cavalcante Lacerda Neto (Julinho)',
        'Kacia Amando da Silva' ,
        'Marcus Andre Pereira de Moura' ,
        'Rivanildo Adones dos Santos' ,
        'Wandebergue Luiz da Silva',
        'Luiz Canavello Neto'
      ]},
      "Bruno Fonseca Guerra":{
        index:8,
        nomes:[
        'Aemerson Rodrigues de Carvalho' ,
        'Ayrton Feitosa Santana' ,
        'Cicero Jose de Sousa',
        'Edilson de Souza Carvalho' ,
        'Hudson Maia de Carvalho',
        'Irineu Antonio da Costa' ,
        'Joilson Lustosa Silva Santana' ,
        'José Emilton Silva de Souza' ,
        'Wagner Sabino de Sousa Silva' ,
      ]},
      "Raimundo Thiago F. Moura de Araújo":{
        index:9,
        nomes:[
        'Eclecio Fernandes da Cunha' ,
        'Edjane Marinho da Costa' ,
        'Everton Silva Santos' ,
        'Fernanda Cinthia Fernandes Dantas',
        'Jonathas de Albuquerque Monteiro Bezerra',
        'José Jubenick Pereira da Silva' ,
        'José Leonilson Jácome Bezerra' ,
        'Lylyam Bibiana de Oliveira Fernandes' ,
        'Suelda Varela Caldas'
      ]},
      "Neydmar Loureiro dos Reis Lima":{
        index:10,
        nomes:[
        'Berenice Damazio Santos',
        'Reginaldo Pereira dos Santos',
        'Izaque Vieira dos Santos',
        'Wesley Santos Aragão',
        'Ana Maria Moura'
      ]}
    }
    this.supervisor_nacional2 = [
      'LUIS ANTÔNIO DA SILVA SOARES',
    ];
    this.array_nacional = [];

    this.supervisor_nacional = {
      "Júlia":{//AL CE SE PI
        index:0,
        nomes:["JOSÉ CLETO DE ARAÚJO ALVES", //AL 
        "PAULO MATOS BARRETO", //CE
        "NEYDMAR LOUREIRO DOS REIS LIMA", //SE
        "BRUNO FONSECA GUERRA", //PI
        ],
      },
      "LUANA BEATRIZ DE JESUS VELOSO FREITAS":{ //MG,ES,BA
        index:1,
        nomes:["DINOVAN QUEIROZ DE ALMEIDA",//MG 
        "DIEGO CAPUCHO CEZANA", //ES
        "BRUNO XAVIER GOMES", //BA
        ],
      },
      "Gabriela":{ //PE,PB,MA,RN
        index:2,
        nomes:["EDMAR DE OLIVEIRA E SILVA", //PE 
        "JULIO BRENO VIEIRA LEMOS", //PB
        "SANDRO SOARES TAVARES", //MA
        "RAIMUNDO THIAGO F. MOURA DE ARAÚJO",//RN
        ],
      },
    }
  }

  public readCsvData() {
    return new Promise ((resolve) => {
      this.http.get('assets/base_local_geral.csv')
      .subscribe(
      data => this.extractData(data),
      err => this.handleError(err)
      );
      resolve();
    });
  }

  private extractData(res) {
    let csvData = res['_body'] || '';
    let parsedData = papa.parse(csvData, {delimiter:";"}).data;
 
    this.headerRow = parsedData[0];
 
    parsedData.splice(0, 1);
    this.csvData = parsedData;
    // console.log(this.headerRow);
    console.log(this.csvData);
    var contador = 0;
    for(let i=0; i < this.csvData.length; i++)
    {
      let dadosEntrevista = {
        cpf:this.csvData[i][this.cpf],
        nome:this.csvData[i][this.nome],
        data:this.csvData[i][this.data],
        quant_horas:this.csvData[i][this.quant_horas],
        animais: [this.csvData[i][this.animais_1], this.csvData[i][this.animais_2], this.csvData[i][this.animais_3]],
        quantidade_animais: [this.csvData[i][this.quantidade_animais_1], this.csvData[i][this.quantidade_animais_2], this.csvData[i][this.quantidade_animais_3]],
        renda_animal: [this.csvData[i][this.renda_animal_1], this.csvData[i][this.renda_animal_2], this.csvData[i][this.renda_animal_3]],
        producao_agropecuaria: {
          producao_animal: this.csvData[i][this.producao_animal],
          der_producao_animal: this.csvData[i][this.der_producao_animal],
          producao_vegetal: this.csvData[i][this.producao_vegetal],
          der_producao_vegetal: this.csvData[i][this.der_producao_vegetal],
          consumo_familiar:this.csvData[i][this.consumo_familiar]
        },
        nao_agricola: this.csvData[i][this.nao_agricola],
        trabalho: {
          temporario_externo: this.csvData[i][this.trabalho_temp_ext],
          permanente_externo: this.csvData[i][this.trabalho_per_ext],
        }, 
        auxilios_recebidos: {
          bolsa_familia: this.csvData[i][this.bolsa_familia],
          emergencia_calamidade:this.csvData[i][this.emergencia_calamidade], 
          seguro_defeso: this.csvData[i][this.seguro_defeso],
          saude_etc:this.csvData[i][this.saude_etc],
          salario_maternidade:this.csvData[i][this.salario_maternidade]
        },
        outros_rendimentos:{
          aposentadoria:this.csvData[i][this.aposentadoria], 
          pensao:this.csvData[i][this.pensao],
          doacao:this.csvData[i][this.doacao],
          aluguel:this.csvData[i][this.aluguel]
        },
        renda_anual_total:this.csvData[i][this.renda_anual_total],
      };
      
      if(i>0)
      {
        contador++;
        if((this.csvData[i][this.cpf_entrevistador] != this.csvData[i-1][this.cpf_entrevistador]) || (this.csvData[i][this.ordem_servico_id] != this.csvData[i-1][this.ordem_servico_id]))
        {
          let temp = new OS(this.csvData[i][this.ordem_servico_id], this.csvData[i][this.concluido_em_branco], this.csvData[i][this.cpf_entrevistador],this.csvData[i][this.nome_entrevistador])
          temp.addInfoEntrevista(dadosEntrevista);
          this.array_os.push(temp);
        }
        else{
          let ult_index = this.array_os.length-1;
          // console.log(this.array_os[ult_index]);
          this.array_os[ult_index].addInfoEntrevista(dadosEntrevista);
        }
      }
      else if(i == 0)
      {
        contador++;
        let temp = new OS(this.csvData[i][this.ordem_servico_id], this.csvData[i][this.concluido_em_branco], this.csvData[i][this.cpf_entrevistador],this.csvData[i][this.nome_entrevistador])
        temp.addInfoEntrevista(dadosEntrevista);
        this.array_os.push(temp);
      }      
    }
    // console.log("contador: ");
    // console.log(contador);
    for(let os of this.array_os)
    {
      os.calcular_entrevistas();
    }

    console.log(this.array_os);
    this.montarRegional().then(() =>{
      this.montarPDFRegional(0);
      this.montarNacionalParcial().then(() =>{
        this.montarPDFNacional(0);
      });
    });
    // this.montarPDF(0);
  }

  private handleError(err) {
    console.log('something went wrong: ', err);
  }
 
  trackByFn(index: any, item: any) {
    return index;
  }

  montarRegional(){
    return new Promise ((resolve) => {
      for(let nome in this.supervisor_estadual){
        let temp = new OS("REGIONAL", "NÃO", "NÃO INFORMADO", nome);
        this.array_regional.push(temp);
      }
      let index;
      let flag_check_nome;
      var self = this;
      for(let elemento of self.array_os){
        flag_check_nome = false;
        let nome_local = elemento.info_os.nome_entrevistador;
        for(let nome in this.supervisor_estadual){
          for(let agente of this.supervisor_estadual[nome].nomes)
          {
            if(agente.toUpperCase() == nome_local)
            {
              if(nome == "Edmar de Oliveira e Silva")
              {
                console.log(nome_local);
              }
              index = this.supervisor_estadual[nome].index;
              flag_check_nome = true;
              let dadosEntrevista = {
                cpf:elemento.info_os.cpf,
                nome:elemento.info_os.nome,
                data:elemento.info_os.data,
                quant_horas:elemento.info_os.quant_horas,
                animais: elemento.info_os.animais,
                quantidade_animais: elemento.info_os.quantidade_animais,
                renda_animal: elemento.info_os.renda_animal,
                producao_agropecuaria: {
                  producao_animal: elemento.info_os.producao_agropecuaria.producao_animal,
                  der_producao_animal: elemento.info_os.producao_agropecuaria.der_producao_animal,
                  producao_vegetal: elemento.info_os.producao_agropecuaria.producao_vegetal,
                  der_producao_vegetal: elemento.info_os.producao_agropecuaria.der_producao_vegetal,
                  consumo_familiar:elemento.info_os.producao_agropecuaria.consumo_familiar,
                },
                nao_agricola: elemento.info_os.nao_agricola,
                trabalho: {
                  temporario_externo: elemento.info_os.trabalho.temporario_externo,
                  permanente_externo: elemento.info_os.trabalho.permanente_externo,
                }, 
                auxilios_recebidos: {
                  bolsa_familia: elemento.info_os.auxilios_recebidos.bolsa_familia,
                  emergencia_calamidade:elemento.info_os.auxilios_recebidos.emergencia_calamidade, 
                  seguro_defeso: elemento.info_os.auxilios_recebidos.seguro_defeso,
                  saude_etc:elemento.info_os.auxilios_recebidos.saude_etc,
                  salario_maternidade:elemento.info_os.auxilios_recebidos.salario_maternidade
                },
                outros_rendimentos:{
                  aposentadoria:elemento.info_os.outros_rendimentos.aposentadoria, 
                  pensao:elemento.info_os.outros_rendimentos.pensao,
                  doacao:elemento.info_os.outros_rendimentos.doacao,
                  aluguel:elemento.info_os.outros_rendimentos.aluguel
                },
                renda_anual_total:elemento.info_os.renda_anual_total,
              };
              this.array_regional[index].addInfoEntrevistaRegional(dadosEntrevista);
              break;
            }
          }
          if(flag_check_nome) break;
        }
      }

      for(let os of self.array_regional)
      {
        os.calcular_entrevistas_regional();
      }
      console.log(this.array_regional);
      resolve();
    });
  }

  montarNacionalParcial(){
    return new Promise ((resolve) => {
      for(let nome in this.supervisor_nacional){
        let temp = new OS("NACIONAL", "NÃO", "NÃO INFORMADO", nome);
        this.array_nacional.push(temp);
      }
      let index;
      let flag_check_nome;
      var self = this;
      for(let elemento of self.array_regional){
        flag_check_nome = false;
        let nome_local = elemento.info_os.nome_entrevistador;
        for(let nome in this.supervisor_nacional){
          for(let agente of this.supervisor_nacional[nome].nomes)
          {
            if(agente.toUpperCase() == nome_local.toUpperCase())
            {
              if(nome == "Edmar de Oliveira e Silva")
              {
                console.log(nome_local);
              }
              index = this.supervisor_nacional[nome].index;
              flag_check_nome = true;
              let dadosEntrevista = {
                cpf:elemento.info_os.cpf,
                nome:elemento.info_os.nome,
                data:elemento.info_os.data,
                quant_horas:elemento.info_os.quant_horas,
                animais: elemento.info_os.animais,
                quantidade_animais: elemento.info_os.quantidade_animais,
                renda_animal: elemento.info_os.renda_animal,
                producao_agropecuaria: {
                  producao_animal: elemento.info_os.producao_agropecuaria.producao_animal,
                  der_producao_animal: elemento.info_os.producao_agropecuaria.der_producao_animal,
                  producao_vegetal: elemento.info_os.producao_agropecuaria.producao_vegetal,
                  der_producao_vegetal: elemento.info_os.producao_agropecuaria.der_producao_vegetal,
                  consumo_familiar:elemento.info_os.producao_agropecuaria.consumo_familiar,
                },
                nao_agricola: elemento.info_os.nao_agricola,
                trabalho: {
                  temporario_externo: elemento.info_os.trabalho.temporario_externo,
                  permanente_externo: elemento.info_os.trabalho.permanente_externo,
                }, 
                auxilios_recebidos: {
                  bolsa_familia: elemento.info_os.auxilios_recebidos.bolsa_familia,
                  emergencia_calamidade:elemento.info_os.auxilios_recebidos.emergencia_calamidade, 
                  seguro_defeso: elemento.info_os.auxilios_recebidos.seguro_defeso,
                  saude_etc:elemento.info_os.auxilios_recebidos.saude_etc,
                  salario_maternidade:elemento.info_os.auxilios_recebidos.salario_maternidade
                },
                outros_rendimentos:{
                  aposentadoria:elemento.info_os.outros_rendimentos.aposentadoria, 
                  pensao:elemento.info_os.outros_rendimentos.pensao,
                  doacao:elemento.info_os.outros_rendimentos.doacao,
                  aluguel:elemento.info_os.outros_rendimentos.aluguel
                },
                renda_anual_total:elemento.info_os.renda_anual_total,
              };
              this.array_nacional[index].addInfoEntrevistaRegional(dadosEntrevista);
              break;
            }
          }
          if(flag_check_nome) break;
        }
      }

      for(let os of self.array_nacional)
      {
        os.calcular_entrevistas_regional();
      }
      console.log(this.array_nacional);
      resolve();
    });
  }

  

  montarNacional(){
    return new Promise ((resolve) => {
      for(let nome of this.supervisor_nacional2){
        let temp = new OS("NACIONAL", "NÃO", "NÃO INFORMADO", nome);
        this.array_nacional.push(temp);
      }
      var self = this;
      for(let elemento of self.array_regional){
        for(let nacional of this.array_nacional){
          // index = this.supervisor_estadual[nome].index;
          // flag_check_nome = true;
          let dadosEntrevista = {
            cpf:elemento.info_os.cpf,
            nome:elemento.info_os.nome,
            data:elemento.info_os.data,
            quant_horas:elemento.info_os.quant_horas,
            animais: elemento.info_os.animais,
            quantidade_animais: elemento.info_os.quantidade_animais,
            renda_animal: elemento.info_os.renda_animal,
            producao_agropecuaria: {
              producao_animal: elemento.info_os.producao_agropecuaria.producao_animal,
              der_producao_animal: elemento.info_os.producao_agropecuaria.der_producao_animal,
              producao_vegetal: elemento.info_os.producao_agropecuaria.producao_vegetal,
              der_producao_vegetal: elemento.info_os.producao_agropecuaria.der_producao_vegetal,
              consumo_familiar:elemento.info_os.producao_agropecuaria.consumo_familiar,
            },
            nao_agricola: elemento.info_os.nao_agricola,
            trabalho: {
              temporario_externo: elemento.info_os.trabalho.temporario_externo,
              permanente_externo: elemento.info_os.trabalho.permanente_externo,
            }, 
            auxilios_recebidos: {
              bolsa_familia: elemento.info_os.auxilios_recebidos.bolsa_familia,
              emergencia_calamidade:elemento.info_os.auxilios_recebidos.emergencia_calamidade, 
              seguro_defeso: elemento.info_os.auxilios_recebidos.seguro_defeso,
              saude_etc:elemento.info_os.auxilios_recebidos.saude_etc,
              salario_maternidade:elemento.info_os.auxilios_recebidos.salario_maternidade
            },
            outros_rendimentos:{
              aposentadoria:elemento.info_os.outros_rendimentos.aposentadoria, 
              pensao:elemento.info_os.outros_rendimentos.pensao,
              doacao:elemento.info_os.outros_rendimentos.doacao,
              aluguel:elemento.info_os.outros_rendimentos.aluguel
            },
            renda_anual_total:elemento.info_os.renda_anual_total,
          };
          nacional.addInfoEntrevistaRegional(dadosEntrevista);
        }
      }

      for(let os of self.array_nacional)
      {
        os.calcular_entrevistas_regional();
      }
      console.log(this.array_nacional);
      resolve();
    });
  }
  

  public montarPDF(itemAtual:number){
    var self = this;
     if(itemAtual < self.array_os.length)
     {
        let row = self.array_os[itemAtual];
        let cabecalho = `RELATÓRIO NÍVEL LOCAL (${row.info_os.nome_entrevistador} OS: ${row.info_os.ordem_servico_id})`;
        // let cabecalho = `RELATÓRIO NÍVEL ESTADUAL (${row.info_os.nome_entrevistador}`;
        let blocoA = `
          A) SINTÉTICO
          
          Nº de entrevistas solicitadas (OS): 
          
          Nº de entrevistas realizadas: ${row.info_os.nome.length}
          
          Proporção da meta atendida (%):
          
          Qual a data de realização de cada entrevista?
          
        `;

        let blocoB = `
          Qual a duração (em minutos) de cada entrevista?
        `;

        let blocoC = `
          Média do tempo de duração das entrevistas (min): ${row.info_os.media_duracao_entrevistas}

          Desvio padrão do tempo de duração das entrevistas: ${row.info_os.desvio_padrao_entrevistas}
          `
        let blocoC2 = ` B) ANALÍTICO/CONSOLIDADO

        Qual a variação do número de animais nas propriedades entrevistadas? Aparecem valores extremos?
      `;

        let blocoD = `
          Qual a renda anual da produção agropecuária das famílias entrevistadas? Aparecem valores extremos?
        `;

        let blocoE = `
          Qual a renda anual da produção não agrícola e do trabalho remunerado das famílias entrevistadas? Aparecem valores extremos?
        `;

        let blocoF = `
          Qual a renda anual dos auxílios recebidos pelas famílias entrevistadas? Aparecem valores extremos?
        `;

        let blocoG = `
          Qual a renda anual de outros rendimentos recebidos pelas famílias entrevistadas? Aparecem valores extremos? 
        `;

        let blocoH = `
          Qual a renda anual total das famílias entrevistadas? Aparecem valores extremos? 
        `;

        self.plotarGrafico(itemAtual, self.array_os).then(() => {
          self.conteudo.push(
            { text: cabecalho, style: 'header', alignment: 'center'  },
            { text: blocoA, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[0], width: 500, alignment: 'center'  },
            { text: blocoB, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[1], width: 500, alignment: 'center'  },
            { text: blocoC, style: 'story', alignment: 'left',pageBreak: 'after'},
            { text: blocoC2, style: 'story', alignment: 'left',},
            { image:this.listaGraficos[2], width: 500, alignment: 'center'   },
            { text: blocoD, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[3], width: 500, alignment: 'center'   },
            { text: blocoE, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[4], width: 500, alignment: 'center'   },
            { text: blocoF, style: 'story', alignment: 'left', pageBreak: 'before'},
            { image:this.listaGraficos[5], width: 500, alignment: 'center'   },
            { text: blocoG, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[6], width: 500, alignment: 'center'   },
            { text: blocoH, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[7], width: 500, alignment: 'center', pageBreak: 'after'   },
            // { text:'', },
          );
            self.listaGraficos = [];
            itemAtual++;
            self.montarPDF(itemAtual);
        });
     }
     else
     {
       this.montarDocDefinition();
     }
  }

  public montarPDFRegional(itemAtual:number){
    var self = this;
     if(itemAtual < self.array_regional.length)
     {
        let row = self.array_regional[itemAtual];
        // let cabecalho = `RELATÓRIO NÍVEL LOCAL (${row.info_os.nome_entrevistador} OS: ${row.info_os.ordem_servico_id})`;
        let cabecalho = `RELATÓRIO NÍVEL ESTADUAL (${row.info_os.nome_entrevistador})`;
        // let cabecalho = `RELATÓRIO NÍVEL NACIONAL (${row.info_os.nome_entrevistador})`;
        let blocoA = `
          A) SINTÉTICO
          
          Nº de entrevistas solicitadas (OS): 
          
          Nº de entrevistas realizadas: ${row.info_os.nome.length}
          
          Proporção da meta atendida (%):
          
          Qual a data de realização de cada entrevista?
          
        `;

        let blocoB = `
          Qual a duração (em minutos) de cada entrevista?
        `;

        let blocoC = `
          Média do tempo de duração das entrevistas (min): ${row.info_os.media_duracao_entrevistas}

          Desvio padrão do tempo de duração das entrevistas: ${row.info_os.desvio_padrao_entrevistas}
          
          Tempo da entrevista mais rápida (min): ${row.info_os.duracao_minima_entrevista}

          Tempo da entrevista mais demorada (min): ${row.info_os.duracao_maxima_entrevista}
          `;

        let blocoC2 = `
        
        B) ANALÍTICO/CONSOLIDADO

        Qual a variação do número de animais nas propriedades entrevistadas? Aparecem valores extremos?
      `;





         

        let blocoD = `
          Qual a renda anual da produção agropecuária das famílias entrevistadas? Aparecem valores extremos?
        `;

        let blocoE = `
          Qual a renda anual da produção não agrícola e do trabalho remunerado das famílias entrevistadas? Aparecem valores extremos?
        `;

        let blocoF = `
          Qual a renda anual dos auxílios recebidos pelas famílias entrevistadas? Aparecem valores extremos?
        `;

        let blocoG = `
          Qual a renda anual de outros rendimentos recebidos pelas famílias entrevistadas? Aparecem valores extremos? 
        `;

        let blocoH = `
          Qual a renda anual total das famílias entrevistadas? Aparecem valores extremos? 
        `;

        self.plotarGrafico(itemAtual, self.array_regional).then(() => {
          self.conteudo.push(
            { text: cabecalho, style: 'header', alignment: 'center'  },
            { text: blocoA, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[0], width: 500, alignment: 'center'  },
            { text: blocoB, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[1], width: 500, alignment: 'center'  },
            { text: blocoC, style: 'story', alignment: 'left', pageBreak: 'after'},
            { text: blocoC2, style: 'story', alignment: 'left',},
            { image:this.listaGraficos[2], width: 500, alignment: 'center'   },
            { text: blocoD, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[3], width: 500, alignment: 'center'   },
            { text: blocoE, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[4], width: 500, alignment: 'center'   },
            { text: blocoF, style: 'story', alignment: 'left', pageBreak: 'before'},
            { image:this.listaGraficos[5], width: 500, alignment: 'center'   },
            { text: blocoG, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[6], width: 500, alignment: 'center'   },
            { text: blocoH, style: 'story', alignment: 'left'},
            { image:this.listaGraficos[7], width: 500, alignment: 'center', pageBreak: 'after'   },
            // { text:'', },
          );
            self.listaGraficos = [];
            itemAtual++;
            self.montarPDFRegional(itemAtual);
        });
     }
     else
     {
       this.montarDocDefinition();
     }
  }

  public montarPDFNacional(itemAtual:number){
    var self = this;
     if(itemAtual < self.array_nacional.length)
     {
        let row = self.array_nacional[itemAtual];
        // let cabecalho = `RELATÓRIO NÍVEL LOCAL (${row.info_os.nome_entrevistador} OS: ${row.info_os.ordem_servico_id})`;
        // let cabecalho = `RELATÓRIO NÍVEL ESTADUAL (${row.info_os.nome_entrevistador})`;
        let cabecalho = `RELATÓRIO NÍVEL NACIONAL (${row.info_os.nome_entrevistador})`;
        let blocoA = `
          A) SINTÉTICO

          Nº de entrevistas solicitadas (OS): 
          Nº de entrevistas realizadas: ${row.info_os.nome.length}
          Proporção da meta atendida (%):
          Qual a data de realização de cada entrevista?
        `;

        let blocoB = `
          Qual a duração (em minutos) de cada entrevista?
        `;

        let blocoC = `
          Média do tempo de duração das entrevistas (min): ${row.info_os.media_duracao_entrevistas}

          Desvio padrão do tempo de duração das entrevistas: ${row.info_os.desvio_padrao_entrevistas}
          
          Tempo da entrevista mais rápida (min): ${row.info_os.duracao_minima_entrevista}

          Tempo da entrevista mais demorada (min): ${row.info_os.duracao_maxima_entrevista}
          `;

        let blocoC2 = `
        B) ANALÍTICO/CONSOLIDADO

        Qual a variação do número de animais nas propriedades entrevistadas? Aparecem valores extremos?
      `;





         

        let blocoD = `
          Qual a renda anual da produção agropecuária das famílias entrevistadas? Aparecem valores extremos?
        `;

        let blocoE = `
          Qual a renda anual da produção não agrícola e do trabalho remunerado das famílias entrevistadas? Aparecem valores extremos?
        `;

        let blocoF = `
          Qual a renda anual dos auxílios recebidos pelas famílias entrevistadas? Aparecem valores extremos?
        `;

        let blocoG = `
          Qual a renda anual de outros rendimentos recebidos pelas famílias entrevistadas? Aparecem valores extremos? 
        `;

        let blocoH = `
          Qual a renda anual total das famílias entrevistadas? Aparecem valores extremos? 
        `;
        if(row.info_os.nome_entrevistador.toUpperCase() == "JÚLIA" || row.info_os.nome_entrevistador.toUpperCase() == "LUIS ANTÔNIO DA SILVA SOARES")
        {
          self.plotarGrafico(itemAtual, self.array_nacional).then(() => {
            self.conteudo.push(
              { text: cabecalho, style: 'header', alignment: 'center'  },
              { text: blocoA, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[0], width: 500, alignment: 'center'  },
              { text: blocoB, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[2], width: 500, alignment: 'center'  },
              { text: blocoC, style: 'story', alignment: 'left', pageBreak: 'after'},
              { text: blocoC2, style: 'story', alignment: 'left',},
              { image:this.listaGraficos[3], width: 500, alignment: 'center',   },
              { text: blocoD, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[4], width: 500, alignment: 'center'   },
              { text: blocoE, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[5], width: 500, alignment: 'center'   },
              { text: blocoF, style: 'story', alignment: 'left', },
              { image:this.listaGraficos[6], width: 500, alignment: 'center'   },
              { text: blocoG, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[7], width: 500, alignment: 'center'   },
              { text: blocoH, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[8], width: 500, alignment: 'center', pageBreak: 'after'   },
              // { text:'', },
            );
              self.listaGraficos = [];
              itemAtual++;
              self.montarPDFNacional(itemAtual);
          });
        }
        else
        {
          self.plotarGrafico(itemAtual, self.array_nacional).then(() => {
            self.conteudo.push(
              { text: cabecalho, style: 'header', alignment: 'center'  },
              { text: blocoA, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[0], width: 500, alignment: 'center'  },
              { text: blocoB, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[1], width: 500, alignment: 'center'  },
              { text: blocoC, style: 'story', alignment: 'left', pageBreak: 'after'},
              { text: blocoC2, style: 'story', alignment: 'left',},
              { image:this.listaGraficos[2], width: 500, alignment: 'center',   },
              { text: blocoD, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[3], width: 500, alignment: 'center'   },
              { text: blocoE, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[4], width: 500, alignment: 'center'   },
              { text: blocoF, style: 'story', alignment: 'left', },
              { image:this.listaGraficos[5], width: 500, alignment: 'center'   },
              { text: blocoG, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[6], width: 500, alignment: 'center'   },
              { text: blocoH, style: 'story', alignment: 'left'},
              { image:this.listaGraficos[7], width: 500, alignment: 'center', pageBreak: 'after'   },
              // { text:'', },
            );
              self.listaGraficos = [];
              itemAtual++;
              self.montarPDFNacional(itemAtual);
          });
        }
        
     }
     else
     {
       this.montarDocDefinition();
     }
  }

  public montarDocDefinition()
  {
    this.docDefinition = {
      content: [
        this.conteudo
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }
    }
  }
  public getDoc()
  {
    return this.docDefinition;
  }

  private plotarGrafico(itemAtual:number, array:any)
  {
    return new Promise ((resolve) => {
      var self = this;
      
      let dataGraficoBlocoA = {
        div:"graficoBlocoA",
        titulo:"",
        subtitulo:"",
        // eixoX: "dinamico conforme tabela TODO",
        eixoY: "Número de entrevistas",
        series: [{
          type: undefined,
          name: 'Número de entrevistas',
          // data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
          data: array[itemAtual].info_os.numero_entrevistas
      }],
    };

    var graficoBlocoA = this.graficos.plotarGraficoLinha(dataGraficoBlocoA);
    // let temp = graficoBlocoA.getSVG();

    let labelHoras = [];
    for(let ent in array[itemAtual].info_os.quant_horas)
    {
      labelHoras.push("Entrevista "+(Number(ent)+1));
    }
    let dataGraficoBlocoB = {
      div:"graficoBlocoB",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      eixoY: "Duração das entrevistas (min)",
      eixoX: labelHoras,
      series: [{
        type: undefined,
        name: 'Duração Entrevistas (min)',
        // data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        data: array[itemAtual].info_os.quant_horas

      }],
    };
    var graficoBlocoB = this.graficos.plotarGraficoBasico("column",dataGraficoBlocoB);

    let boxplotC = [];
    for(let qtd_animais of array[itemAtual].info_os.quantidade_animais)
    {
      boxplotC.push(this.calcularBoxPlot(qtd_animais));
    }

    let seriesOnly = [];
    let outlier = [];

    boxplotC.forEach((elemento, index) =>{
      seriesOnly.push(elemento[0]);
      if(elemento[1].length > 0)
      {
        elemento[1].forEach((out) => {
          outlier.push([index,out]);
        });
      }
      else
      {
        outlier.push([]);
      }
      
    });
    
    
    let dataGraficoBlocoC = {
      div:"graficoBlocoC",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      eixoY: "Número de cabeças",
      eixoX: array[itemAtual].info_os.animais,
      data: seriesOnly,
      outlier:outlier,
  };

  var graficoBlocoC = this.graficos.plotarGraficoBoxplot("boxplot",dataGraficoBlocoC);
  let boxplotD = [];
  boxplotD.push(this.calcularBoxPlot(array[itemAtual].info_os.producao_agropecuaria.producao_animal));
  boxplotD.push(this.calcularBoxPlot(array[itemAtual].info_os.producao_agropecuaria.der_producao_animal));
  boxplotD.push(this.calcularBoxPlot(array[itemAtual].info_os.producao_agropecuaria.producao_vegetal));
  boxplotD.push(this.calcularBoxPlot(array[itemAtual].info_os.producao_agropecuaria.der_producao_vegetal));
  boxplotD.push(this.calcularBoxPlot(array[itemAtual].info_os.producao_agropecuaria.consumo_familiar));

  let seriesOnlyD = [];
  let outlierD = [];

  boxplotD.forEach((elemento, index) =>{
    seriesOnlyD.push(elemento[0]);
    if(elemento[1].length > 0)
    {
      elemento[1].forEach((out) => {
        outlierD.push([index,out]);
      });
    }
    else
    {
      outlier.push([]);
    }
    
  });

  // let outlierD = [this.formatarOutlier(boxplotD[0][1], 0), this.formatarOutlier(boxplotD[1][1], 0), this.formatarOutlier(boxplotD[2][1], 0), this.formatarOutlier(boxplotD[3][1], 0), this.formatarOutlier(boxplotD[4][1], 0)];

  let dataGraficoBlocoD = {
    div:"graficoBlocoD",
    titulo:"",
    subtitulo:"",
    // eixoX: "dinamico conforme tabela TODO",
    eixoY: "Valores(R$)",
    eixoX: ["Produção animal", "Der. produção animal", "Produção vegetal", "Der. produção vegetal", "consumo familiar"],
    data: seriesOnlyD,
    outlier:outlierD,
  };

    var graficoBlocoD = this.graficos.plotarGraficoBoxplot("boxplot",dataGraficoBlocoD);

    let boxplotE = [];
    boxplotE.push(this.calcularBoxPlot(array[itemAtual].info_os.nao_agricola));
    boxplotE.push(this.calcularBoxPlot(array[itemAtual].info_os.trabalho.temporario_externo));
    boxplotE.push(this.calcularBoxPlot(array[itemAtual].info_os.trabalho.permanente_externo));
    
    let seriesOnlyE = [];
    let outlierE = [];

    boxplotE.forEach((elemento, index) =>{
    seriesOnlyE.push(elemento[0]);
    if(elemento[1].length > 0)
    {
      elemento[1].forEach((out) => {
        outlierE.push([index,out]);
      });
    }
    else
    {
      outlierE.push([]);
    }

    });

  let dataGraficoBlocoE = {
    div:"graficoBlocoE",
    titulo:"",
    subtitulo:"",
    // eixoX: "dinamico conforme tabela TODO",
    eixoY: "Valores(R$)",
    eixoX: ['Produção não agrícola', 'Trab. temp. externo', 'Trab. perm. externo'],
    data: seriesOnlyE,
    outlier:outlierE,
};

    var graficoBlocoE = this.graficos.plotarGraficoBoxplot("boxplot",dataGraficoBlocoE);

    let boxplotF = [];
    boxplotF.push(this.calcularBoxPlot(array[itemAtual].info_os.auxilios_recebidos.bolsa_familia));
    boxplotF.push(this.calcularBoxPlot(array[itemAtual].info_os.auxilios_recebidos.emergencia_calamidade));
    boxplotF.push(this.calcularBoxPlot(array[itemAtual].info_os.auxilios_recebidos.seguro_defeso));
    boxplotF.push(this.calcularBoxPlot(array[itemAtual].info_os.auxilios_recebidos.salario_maternidade));
    boxplotF.push(this.calcularBoxPlot(array[itemAtual].info_os.auxilios_recebidos.saude_etc));
    
    let seriesOnlyF = [];
    let outlierF = [];

    boxplotF.forEach((elemento, index) =>{
    seriesOnlyF.push(elemento[0]);
    if(elemento[1].length > 0)
    {
      elemento[1].forEach((out) => {
        outlierF.push([index,out]);
      });
    }
    else
    {
      outlierF.push([]);
    }

    });

    let dataGraficoBlocoF = {
      div:"graficoBlocoF",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      eixoY: "Valores(R$)",
      eixoX: ["Bolsa Família", "Aux. emergência", "Seguro defeso", "Salário maternidade", "Outros"],
      data: seriesOnlyF,
      outlier:outlierF,
    };

    var graficoBlocoF = this.graficos.plotarGraficoBoxplot("boxplot",dataGraficoBlocoF);

    let boxplotG = [];
    boxplotG.push(this.calcularBoxPlot(array[itemAtual].info_os.outros_rendimentos.aposentadoria));
    boxplotG.push(this.calcularBoxPlot(array[itemAtual].info_os.outros_rendimentos.pensao));
    boxplotG.push(this.calcularBoxPlot(array[itemAtual].info_os.outros_rendimentos.doacao));
    boxplotG.push(this.calcularBoxPlot(array[itemAtual].info_os.outros_rendimentos.aluguel));
    
    let seriesOnlyG = [];
    let outlierG = [];

    boxplotG.forEach((elemento, index) =>{
    seriesOnlyG.push(elemento[0]);
    if(elemento[1].length > 0)
    {
      elemento[1].forEach((out) => {
        outlierG.push([index,out]);
      });
    }
    else
    {
      outlierG.push([]);
    }

    });

    let dataGraficoBlocoG = {
      div:"graficoBlocoG",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      eixoY: "Valores(R$)",
      eixoX: ["Aposentadoria", "Pensão", "Doação", "Aluguel e arrendamento"],
      data: seriesOnlyG,
      outlier:outlierG,
    };

    var graficoBlocoG = this.graficos.plotarGraficoBoxplot("boxplot",dataGraficoBlocoG);

    let boxplotH = this.calcularBoxPlot(array[itemAtual].info_os.renda_anual_total);
    let outlierH = this.formatarOutlier(boxplotH[1], 0);

    let dataGraficoBlocoH = {
      div:"graficoBlocoH",
      titulo:"",
      subtitulo:"",
      // eixoX: "dinamico conforme tabela TODO",
      eixoY: "Valores(R$)",
      eixoX: ["Renda anual"],
      data: [boxplotH[0]],
      outlier:outlierH,
    };

    var graficoBlocoH = this.graficos.plotarGraficoBoxplot("boxplot",dataGraficoBlocoH);

    let array_plots = [graficoBlocoA, graficoBlocoB, graficoBlocoC, graficoBlocoD, graficoBlocoE, graficoBlocoF, graficoBlocoG, graficoBlocoH];
    this.graficos.converterSVGtoPNG(array_plots, 0).then((data) =>{
      //  console.log(data);
      this.listaGraficos = data;
      resolve();
    });
            

  });

  }

  private formatarOutlier(array:any, index:number)
  {
    let temp = [];
    for(let arr of array)
    {
      temp.push([index,arr]);
    }
    return temp;
  }

  private calcularBoxPlot(array_data:any)
  {
    let array_ordenado = [];
    let min:number;
    let max:number;
    let q1:number;
    let mediana:number;
    let q3:number;
    let lim_inferior:number;
    let lim_superior:number; 
    let len:number;

    let subArray1:any;
    let subArray2:any;

    let outlier = [];
    array_ordenado = (array_data.sort((a,b) => a-b));
    // console.log(array_ordenado);
    len = array_ordenado.length;

    let min_old = array_ordenado[0];
    let max_old = array_ordenado[len-1];

    mediana = this.calcularQuarter(array_ordenado);

    //se o array for par
    if(len > 1)
    {
      if(len%2 == 0)
      {
        subArray1 = array_ordenado.slice(0, (len/2));
        subArray2 = array_ordenado.slice(len/2, len); 
      }
      else
      {
        let up = Math.ceil(len/2)-1;
        subArray1 = array_ordenado.slice(0, up);
        subArray2 = array_ordenado.slice(up+1, len);
      }
    }
    else
    {
      subArray1 = array_ordenado;
      subArray2 = array_ordenado;
    }

    q1 = this.calcularQuarter(subArray1);
    q3 = this.calcularQuarter(subArray2);

    lim_inferior = q1 - 1.5*(q3-q1);
    lim_superior = q1 + 1.5*(q3-q1);

    for(let elemento of array_ordenado)
    {
      if(elemento > lim_superior || elemento < lim_inferior)
      {
        outlier.push(elemento);
      }
    }

    // let media = array_ordenado.reduce((total, valor) => total+(valor)/array_ordenado.length, 0);
    // let variancia = array_ordenado.reduce((total, valor) => total + Math.pow(media - (valor), 2)/array_ordenado.length, 0);
    // let desvioPadrao = Math.sqrt(variancia);

    // min = media - 1.96*desvioPadrao;
    // max = media + 1.96*desvioPadrao;
    return [[lim_inferior, q1, mediana, q3, lim_superior], outlier];
  }

  private calcularQuarter(array:any)
  {
    let len = array.length;
    let mediana:number;
    //se for par
    if(len%2 == 0)
    {
      mediana = (array[len/2] + array[(len/2)-1])/2;
    }
    else
    {
      mediana = array[Math.ceil(len/2)-1];
    }

    return mediana
  }
}

import { Injectable } from '@angular/core';

// import * as HighCharts from 'highcharts';

import { Chart } from 'angular-highcharts'

declare var require: any;
let hcharts = require('highcharts');
require('highcharts/modules/exporting')(hcharts);
require('highcharts/highcharts-more.js')(hcharts);


/*
  Generated class for the GraficosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GraficosProvider {
  image: HTMLImageElement;

  array_dados = [];
  constructor() {
    // console.log('Hello GraficosProvider Provider');
  }

  public plotarGraficoBasico(tipo:string, data:any)
  {
    let temp = hcharts.chart({
      chart: {
        styledMode: true,
        type: tipo,
        renderTo: data.div
      },
      title: {
        text: data.titulo
      },
      xAxis: {
        categories: data.eixoX,
        labels: {
          style: {
            fontSize:'16px',
            break: 'break-word',
            // color: 'red'
          }
        }
      },
      yAxis: {
        max: 100,
        title: {
          text: data.eixoY,
          style: {
            // color: 'red',
            fontSize:'20px'
          }
        },
        labels: {
          style: {
            // color: 'red',
            fontSize:'16px'
          }
        }
      },
      credits: {
        enabled: false
    },  
      series: data.series
    });
    
    return temp;
    // temp.getSVG();
  
  }

  public plotarGraficoPizza(data:any)
  {
    let temp = hcharts.chart({
      chart: {
        type: 'pie',
        renderTo: data.div
      },
      title: {
        text: data.titulo
      },
      credits: {
        enabled: false
    },
    plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
          }
      }
    },  
      series: data.series
    });    
    return temp;
    // temp.getSVG();
  
  }

  graficoTeste()
  {
    var seriesData = [["apple",29.9], ["orange",71.5], ["mango",106.4]];     
    let temp = hcharts.chart("graficoBlocoA", {
      xAxis: {
        tickInterval: 1,
        labels: {
            enabled: true,
            formatter: function() { return seriesData[this.value][0];},
        }
    },

    series: [{
        data: seriesData     
     }]
 
    });
  }

  plotarGraficoLinha(data:any)
  {
    let temp = hcharts.chart(data.div, {

      title: {
          text: data.titulo
      },
  
      subtitle: {
          text: data.subtitulo
      },
  
      yAxis: {
          title: {
              text: data.eixoY,
              style: {
                // color: 'red',
                fontSize:'20px'
            }
          },
          labels: {
            style: {
              // color: 'red',
              fontSize:'18px'
          }
        }
      },
      xAxis: {
        tickInterval: 1,
        labels: {
            enabled: true,
            formatter: function() { return data.series[0].data[this.value][0];},
            style: {
              // color: 'red',
              fontSize:'20px'
          }
        }
      },
      
      legend: {
        itemStyle: {
          fontSize:'18px',
        }
      },
      credits: {
        enabled: false
    },  
      // plotOptions: {
      //     series: {
      //         label: {
      //             connectorAllowed: true
      //         },
      //         // pointStart: 2010//Ajustar ou remover no teste final
      //     }
      // },
  
      series: data.series,
  
      responsive: {
          rules: [{
              condition: {
                  maxWidth: 500
              },
              chartOptions: {
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  }
              }
          }]
      }
  
    });
    return temp;
  }

  plotarGraficoBoxplot(tipo:string, data:any)
  {
    let temp = hcharts.chart(data.div, {

      chart: {
          type: 'boxplot'
      },
  
      title: {
          text: data.titulo
      },
  
      legend: {
          enabled: false
      },
  
      xAxis: {
          categories: data.eixoX,
          labels: {
            style: {
              fontSize:'16px',
              // color: 'red'
            }
          }
      },
      credits: {
        enabled: false
      },  
      yAxis: {
          title: {
              text: data.eixoY,
              style: {
                // color: 'red',
                fontSize:'20px'
              }
          },
          labels: {
            style: {
              // color: 'red',
              fontSize:'16px'
            }
          }
          // plotLines: [{
          //     value: 932,
          //     color: 'red',
          //     width: 1,
          //     label: {
          //         text: 'Theoretical mean: 932',
          //         align: 'center',
          //         style: {
          //             color: 'gray'
          //         }
          //     }
          // }]
      },
  
      series: [{
          type: undefined,
          name: 'Observações',
          data: data.data,
          // tooltip: {
          //     headerFormat: '<em>Experiment No {point.key}</em><br/>'
          // }
      }, 
      {
          name: 'Outlier',
          color: hcharts.getOptions().colors[0],
          type: 'scatter',
          data: data.outlier,
          marker: {
              fillColor: 'white',
              lineWidth: 1,
              lineColor: hcharts.getOptions().colors[0]
          },
          tooltip: {
              pointFormat: 'Observation: {point.y}'
          }
      }
    ]
  
  });
    return temp;
  }
  converterSVGtoPNG(array_chart:any, index:number)
  {
    return new Promise ((resolve) => {
      var self = this;
      if(index == 0){
        this.array_dados = [];
      }
      var chart = array_chart[index];
        var svg = chart.getSVG({
          exporting: {
              sourceWidth: chart.chartWidth,
              sourceHeight: chart.chartHeight
          }
        });
        var render_width = 1000;
        var render_height = render_width * chart.chartHeight / chart.chartWidth
        var canvas = document.createElement('canvas');
        canvas.height = render_height;
        canvas.width = render_width;
        
        // self.image = new Image;
        // // descomentar:
        // self.image.onload = () => {
        //     canvas.getContext('2d').drawImage(self, 0, 0, render_width, render_height);
        //     var data = canvas.toDataURL("image/png")
        //     // console.log(data);
        //     resolve(data);
        // };
        // self.image.src = 'data:image/svg+xml;base64,' + window.btoa(svg);
  
        var ctx = canvas.getContext('2d');
  
        var img = document.createElement('img');
        img.setAttribute('src', 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svg))));
        img.onload = function() {
            ctx.drawImage(img, 0, 0, render_width, render_height);
            var data = canvas.toDataURL('image/png');
            // console.log(data);
            self.array_dados.push(data);
            index++;
            if(index == (array_chart.length )) {
              resolve(self.array_dados);
            }
            else {
              self.converterSVGtoPNG(array_chart, index).then((data)=>{
                resolve(data);
              });
            }
        };
      
      // if(index == array_chart.length){
      //   resolve(self.array_dados);
      // }
    });
    
  }

  public teste(){
    return "teste"
  }

  public plotarGraficoStackedBar(data:any)
  {
    let temp = hcharts.chart({
      chart: {
        styledMode: true,
        type: 'column',
        renderTo: data.div
      },
      title: {
        text: data.titulo
      },
      credits: {
        enabled: false
    },
    xAxis: {
      categories: data.xAxis,
      labels: {
        style: {
          fontSize:'16px',
          break: 'break-word',
          // color: 'red'
        }
      }
    },
    yAxis: {
      max: 100,
      // className: 'highcharts-color-0',
      title: {
          text: data.yAxis
      },
      labels: {
        style: {
          // color: 'red',
          fontSize:'16px'
        }
      },
      stackLabels: {
          enabled: false,
          style: {
              fontWeight: 'bold',
              // color: (hcharts.theme && hcharts.theme.textColor) || 'gray'
          }
      }
  },
  plotOptions: {
    column: {
        stacking: 'normal',
        dataLabels: {
            enabled: false,
            // color: (hcharts.theme && hcharts.theme.dataLabelsColor) || 'white'
        }
    }
  },
      series: data.series
    });    
    return temp;
    // temp.getSVG();
  
  }



}
